<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"/>

	<div class="w3-bar" style="background:#bdcebe">
		<div class="w3-col m8">
			<a href="/EtuJob/home" class="w3-bar-item w3-button w3-hover-none w3-bottombar w3-hover-border-purple">Home</a>
			<a href="/EtuJob/requests?id=${sessionScope.connectedId }" class="w3-bar-item w3-button w3-hover-none w3-bottombar w3-hover-border-purple">My Requests</a>
			<a href="/EtuJob/profile" class="w3-bar-item w3-button w3-hover-none  w3-bottombar w3-hover-border-purple">Manage Profile</a>
			<a href="/EtuJob/conversations" class="w3-bar-item w3-button w3-hover-none w3-bottombar w3-hover-border-purple">Messages</a>
			
			<!--  a href="#" class="w3-bar-item w3-button w3-hover-none w3-border-white w3-bottombar w3-hover-border-black">Account Settings</a-->
		</div>
		<div class="w3-col m3">
			<c:choose>
				    <c:when test="${ !empty sessionScope.firstname && !empty sessionScope.lastname }">
				    	<form action="logout" method="POST">
				    		<p style="font-weight: bold;">Welcome <c:out value="${sessionScope.firstname}"/> <c:out value="${sessionScope.lastname}"/>! <input class="w3-btn w3-red w3-round-xxlarge" type="submit" value="Logout"/></p>				    	
				    	</form>
				    </c:when>
				    <c:otherwise><a href="/EtuJob/login" class="w3-bar-item w3-button w3-hover-none w3-bottombar w3-hover-border-purple">Login</a></c:otherwise>
			</c:choose>
		</div>
	</div> 

