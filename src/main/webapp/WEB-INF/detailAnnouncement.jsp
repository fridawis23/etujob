<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"/>
<title>Announcement detail</title>
</head>
<body>
  <!-- Header -->
<%@ include file="header.jsp" %>
<%@ include file="menu.jsp" %>
<%
		if(session.getAttribute("firstname")==null){
			response.sendRedirect(request.getContextPath() + "/login");
		}
%>





<div  class="w3-container w3-center">
<h2>Announcement detail</h2>
</div>

<div class="w3-row">
	<p class="w3-text-red"><c:out value="${alertMessage }"/></p>
</div>

<div style="margin-left:260px ">
      <p><c:out value="${ announcement.service.serviceTitle}" /></p>
 </div>
 
 <div class="w3-container w3-right" style="margin-right:260px ">
 <!--<div class="w3-col m1 w3-row-padding">
    <a href="/EtuJob/ProfilAnnouncement?id=${announcement.service.student.id}" class="w3-button w3-green">Voir profil de l'annonceur</a>
 </div>!-->
  <div class="w3-dropdown-hover" >View Publisher Profil
    <div class="w3-dropdown-content w3-card-4" style="width:250px">
      <img src="${pageContext.request.contextPath}/img/logo_etujob_teal.png" alt="Profil" style="width:100%">
      <div class="w3-container">
        
      <h3><c:out value="${announcement.service.student.lastname }" /> <c:out value="${announcement.service.student.firstname }" /></h3>
      <p>Experience:  <c:out value="${announcement.service.student.profile }" /></p>
      <p>Email: <c:out value="${announcement.service.student.email }" /></p>
      <p>Phone: <c:out value="${announcement.service.student.phone }" /></p>
      </div>
    </div>
  </div>

</div>	
	
<div style="margin-left:20px"> 
 <div  style="width:40%" >

    <p>FirstName: <c:out value="${ announcement.service.student.firstname}" /></p>  
    <p>LastName: <c:out value="${ announcement.service.student.lastname}" /></p>
    <p>Published on: <c:out value="${ announcement.published_date}" /></p>
    <p>DESCRIPTION: </p><p class="text-start"><c:out value="${ announcement.service.serviceDescription}" /></p>
    <h4><p>Rate: <c:out value="${ announcement.service_price}" /> €</p></h4>
    <p>schedule available: <c:out value="${ announcement.announcement_date }" /></p>
    
 </div>

<c:if test= "${connectedStatus < 3}">
  <div class="w3-row" style="color:blue;">
     <a href="/EtuJob/message?s=${sessionScope.connectedId}&r=${announcement.service.student.id}&mt=${announcement.service.serviceTitle}" >Contact announcer</a>
     <a href="/EtuJob/report?s=${sessionScope.connectedId}&id=${announcement.announcementId}&mt=${announcement.service.serviceTitle}">Repost announcement</a>
     <a href="/EtuJob/comment?id=${sessionScope.connectedId}&s=${announcement.service.serviceId } ">Add comment</a>
   
     <form action="contrat" method="POST">
     	<input type="hidden" name="announcementId" value="${ announcement.announcementId}">
     	<input type="hidden" name="contractorId" value="${sessionScope.connectedId}">
     	<input type="hidden" name="announcementTitle" value="<c:out value="${ announcement.service.serviceTitle}" />">
     	<input type="hidden" name="publisherId" value="<c:out value="${ announcement.service.student.id}" />"/>
     	<input type="hidden" name="requestNote" value="">
     	<input type="hidden" name="requestDate" value="">
     	<input type="hidden" name="validationDate" value="">
     	<input type="submit" value="Contract the offer">
     </form>
 </div>
 </c:if>
 
</div>
  
	
<!-- footer -->
<%@  include file="footer.jsp" %>


  

</body>
</html>