
  <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
  <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 
<!DOCTYPE html>
<html>
<head>
	<meta meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"/>
	<title>Manage Services</title>
</head>
<body style="min-height:100vh;">
<!-- Header -->
<%@ include file="header.jsp" %>
<%@ include file="menu.jsp" %>

<%
	if(session.getAttribute("firstname")==null ){
		response.sendRedirect(request.getContextPath() + "/login");
	}
	else if(!session.getAttribute("connectedStatus").toString().equals("1"))
		response.sendRedirect(request.getContextPath() + "/profile");
%>

<div class="w3-container">
	<div class="w3-col m9 w3-light-grey w3-margin">
		<div class="w3-col m6 w3-margin">
		<div class="w3-row">
			<p class="w3-text-red"><c:out value="${alertMessage }"/></p>
		</div>
		<form class="w3-container w3-card-4 w3-light-grey w3-text-blue w3-margin" method="POST" action="publish?id=${service.serviceId}">
			<h3>Publish a service (one-shot)</h3>
				<!-- Objet Service -->
				<!-- label>Service ID: <span class="w3-text-red">${service.serviceId }</span></label></br -->
		
				<input type="hidden" name="publishType" value="oneshot" />
				<input class="w3-input w3-border-0" type="hidden" value="${service.serviceId }" name="serviceId"/>
				
				<label>Service Title</label>
				<input class="w3-input w3-border-0" type="text" value="${service.serviceTitle }" name="serviceTitle" disabled/>
				
				<label>Service Description</label>
				<textarea class="w3-input w3-border-0" type="text" rows="5" name="serviceDescription" disabled>
					${service.serviceDescription }
				</textarea>
				
				<label>Service Duration:<span class="w3-text-red">${service.serviceDuration}</span></label></br>
				<input class="w3-input w3-border-0" type="hidden" value="${service.serviceDuration }" name="serviceDuration"/>
				
				<label>Service Category</label>
				<input class="w3-input w3-border-0" type="text" value="${service.serviceCategory.categoryName}" name="serviceCategory" disabled/>
				<!-- fin Objet -->
				
				<label>Price</label>
				(&euro;)<input class="w3-input w3-border-0" placeholder="0.00 &euro;" required type="number" step="0.01" name="servicePrice"/>
				
				<label>Time Slot</label>
				<input class="w3-input w3-border-0" min="" max="" type="datetime-local" required name="serviceTimeSlot"/>
				
				<input  type="hidden" value="0" name="contractorId"/>
				
				<input class="w3-btn w3-teal w3-hover-purple w3-margin" type="submit" value="Publish one-shot"/>
		</form>
		</div>
		<!-- 
		<div class="w3-col m4 w3-margin">
		<form class="" method="POST" action="publish">
			<h3>Or define a regular schedule</h3> -->
				<!-- Objet Service -->
				<!-- label>Service ID: <span class="w3-text-red">${service.serviceId }</span></label></br -->
				<!-- <input type="hidden" name="publishType" value="regular"/>
				<input class="w3-input w3-border-0" type="hidden" value="${service.serviceId }" name="serviceId"/>
				<input class="w3-input w3-border-0" type="hidden" value="${service.serviceTitle }" name="serviceTitle"/>
				<input class="w3-input w3-border-0" type="hidden" width="80" height="80" value="${service.serviceDescription }" name="serviceDescription"/>
				<input class="w3-input w3-border-0" type="hidden" value="${service.serviceDuration }" name="serviceDuration"/>
				<input class="w3-input w3-border-0" type="hidden" value="${service.serviceCategory.categoryName}" name="serviceCategory"/>
				 fin Objet 
				
				<label>Time Slot</label>
				<input class="w3-input w3-border-0" min="" type="datetime-local" name="serviceTimeSlot"/>
				
				<label>Price</label>
				<input class="w3-input w3-border-0" placeholder="0.00 $" type="number" step="0.01" name="servicePrice"/>
				
				<input  type="hidden" value="0" name="contractorId"/>
				<input class="w3-btn w3-teal w3-hover-purple w3-margin" type="submit" value="define schedule"/>
		</form>
		</div>
	 -->
	</div>
	
</div>
<!-- footer -->
<%@  include file="footer.jsp" %>
</body>
</html>