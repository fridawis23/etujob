
  <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 
<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
   <meta charset="UTF-8">
<title>Create Service</title>
</head>
<body style="min-height:100vh;">
<!-- Header -->
<%@ include file="header.jsp" %>
<%@ include file="menu.jsp" %>

<%
	if(session.getAttribute("firstname")==null ){
		response.sendRedirect(request.getContextPath() + "/login");
	}
	else if(!session.getAttribute("connectedStatus").toString().equals("1"))
		response.sendRedirect(request.getContextPath() + "/profile");
%>
<div class="w3-container">

	<h2>Create a service</h2>
	
	<div class="w3-col m4">
		<form class="w3-container w3-card-4 w3-light-grey w3-text-blue w3-margin" method="POST" action="create_service">
			
			<label>Service Title</label>
			<div class="w3-row w3-section">	 
				<div class="w3-rest">
					<input class="" type="text" name="serviceTitle" required/>
				</div>
			</div>
			<label>Service Description</label>
			<div class="w3-row w3-section">	 
				<div class="w3-rest">
					<textarea class="" type="text" name="serviceDescription" required></textarea>
				</div>
			</div>
			<label>Service Duration:</label>
			<div class="w3-row w3-section">	 
				<div class="w3-rest">
					<input class="" type="time" name="serviceDuration" required/>
				</div>
			</div>
			<div class="w3-row w3-section">	 
				<div class="w3-rest">
					<select class="w3-select" name="serviceCategory" required>
						<option value="" disabled selected>Choose a category</option>
						<c:forEach items="${categories}" var="categories" varStatus="status">
							<option value="${categories.categoryId}">${categories.categoryName}</option>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="w3-row w3-section">	 
				<div class="w3-rest">
					<button class="w3-btn  w3-block w3-round-large w3-teal w3-hover-purple">Create</button>
				</div>
			</div>			
		</form>
	</div>

	
</div>
<!-- footer -->
<%@  include file="footer.jsp" %>
</body>
</html>