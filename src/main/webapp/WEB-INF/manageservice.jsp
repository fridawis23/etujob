
  <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 
<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
   <meta charset="UTF-8">
<title>Manage Services</title>
</head>
<body style="min-height:100vh;">
<!-- Header -->
<%@ include file="header.jsp" %>
<%@ include file="menu.jsp" %>

<div class="w3-container">
	<h2>Manage Services</h2>
	  
	<div class="w3-row">
		<div class="w3-col m4">
			<a class="w3-btn w3-green"><span class="glyphicon glyphicon-edit">New Service</span></a>
		</div>
		<div class="w3-col m4">
			<a class="w3-btn w3-green">View my Publications</a>
		</div>
	</div>
	<div class="w3-row w3-col m10" style="margin-left:auto;margin-right:auto;">
		<table class="w3-table">
			<thead>
				<th>Service Title</th>
				<th>Category</th>
				<th>Duration</th>
				<th>Edit</th>
				<th>Publish</th>
				<th>Delete</th>
			</thead>
			<tbody>
				<c:forEach items="${ services}" var="servicesItem" varStatus="status">
					<tr>
						<td>${servicesItem.serviceTitle}</td>
						<td><c:out value="${servicesItem.serviceCategory.categoryName }"/></td>
						<td><c:out value="${servicesItem.serviceDuration }"/></td>
						<td><a href="editservice?id=${servicesItem.serviceId }"><span class=" glyphicon glyphicon-pencil"></span></a></td>
						<td><a href="publish?id=${servicesItem.serviceId }"><span class="glyphicon glyphicon-ok"></span></a></td>
						<td><a href="deleteservice?id=${servicesItem.serviceId }"><span class="glyphicon glyphicon-remove"></span></a></td>			
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>

<!-- footer -->
<%@  include file="footer.jsp" %>
</body>
</html>