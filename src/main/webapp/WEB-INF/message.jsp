<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
   <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Messagerie</title>
</head>
<body>
	<!-- Header -->
<%@ include file="header.jsp" %>
<%@ include file="menu.jsp" %>
<%
		if(session.getAttribute("firstname")==null){
			response.sendRedirect(request.getContextPath() + "/login");
		}
		try{
			String connectedId = session.getAttribute("connectedId").toString();
		}catch(Exception e){
			e.printStackTrace();
		}
%>
	<div class="w3-container">
		<a href="/EtuJob/conversations" style="text-decoration:none;"><h2>&#x25c1;</h2></a>
		<h3>Messagerie</h3>
	

		<c:forEach items="${ messages_discussion }" var="message" varStatus="status">
			<div class="w3-row" style="">
				<c:choose>
				    <c:when test="${ message.sender.id == connectedId }">
					   <div class="w3-col m4 w3-light-gray" style="margin-left:300px; margin-bottom:3px">
							<h5 class="w3-pale-green"><c:out value="${ message.sender.firstname}" />/ <c:out value="${ message.messageTitle}"/></h5>
							<p class=""><c:out value="${ message.messageContent}" /></p>
							<p class=""><c:out value="${ message.messageDateTime}" /></p>
						</div>
				    </c:when>
				    <c:otherwise>
						<div class="w3-col m4 w3-pale-yellow" style="margin-bottom:3px">
							<h5 class="w3-pale-green"><c:out value="${ message.sender.firstname}" />/ <c:out value="${ message.messageTitle}"/></h5>
							<p class=""><c:out value="${ message.messageContent}" /></p>
							<p class=""><c:out value="${ message.messageDateTime}" /></p>
						</div>
				    </c:otherwise>
				</c:choose>
			</div>
		</c:forEach>
		
		<div class="w3-yellow">
			<form class="w3-col m7 w3-margin" action="message" method="POST">
				<div class="w3-col m6">
					<h5 class="w3-row">new message</h5>
					<input type="hidden" value="<c:out value="${ senderId}"/>" name="s"/>
					<input type="hidden" value="<c:out value="${ receiverId}"/>" name="r"/>
					<input type="hidden" name="mt" value="${ msgTitle}"/>
					<input class="w3-margin" type="text"  value="${ msgTitle}" disabled/>
					<textarea class="w3-input w3-light-gray w3-border-0 w3-margin" rows="4" cols="20" id="reply" name="reply"></textarea>
					<input class="w3-button w3-purple" style="margin-left:250px; border-color:purple" type="submit" value="Send &#x27a4;"/>
				</div>		
			</form>
	</div>
	
	</div>
	
	<!-- footer -->
	<%@ include file="footer.jsp" %>
</body>
</html>































