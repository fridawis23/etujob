
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"/>
<meta charset="UTF-8">
<title>Password Reset</title>
</head>

<body>
	<%@ include file="header.jsp" %>
	<div class="w3-center" style="margin:auto;">
		
		<form class="w3-container" method="POST" action="passwordreset">
			<div style="margin-bottom:6px;">
				<h3>Setting-up new Password</h3>
			</div>
			<p class="w3-text-red">${ alertMessage }</p>
			<input type="hidden" name="id" value="${ loginId }"/>
			<input type="hidden" name="c" value="${ uniqueCode }"/>
			<input style="margin: 4px;" type="password" name="password1" placeholder="new password"/>
			<br><input style="margin: 4px;" type="password" name="password2" placeholder="retype password"/>
			<br><input class="w3-button w3-hover-purple" style="margin: 4px; background:#bdcebe" type="submit" value="Reset"/>
		</form>
	</div>
</body>
</html>