<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<meta charset="UTF-8">
<title>Messages</title>
</head>
<body>
	<!-- Page header -->
	<%@ include file="header.jsp" %>
	<%@ include file="menu.jsp" %>
	<%
		if(session.getAttribute("firstname")==null){
			response.sendRedirect(request.getContextPath() + "/login");
		}
		try{
			String connectedId = session.getAttribute("connectedId").toString();
			if(session.getAttribute("connectedStatus").toString().equals("3")){
				response.sendRedirect(request.getContextPath() + "/home");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	%>
	
	<div class="w3-container">
	<h3>Conversations</h3>
		<div class="w3-center" style="margin:auto;">
			<table class="w3-table w3-col m6">
				<c:forEach items="${ messages_list }" var="message" varStatus="status">
					<tr class="w3-hover-purple">
						<td>
							&#x2709;
						</td>
						<td>
							<c:choose>
								<c:when test="${ message.sender.id eq Integer.valueOf(connectedId) }">
									<c:out value="${ message.receiver.firstname}" />
									<c:out value="${ message.receiver.lastname}" />
								</c:when>
								<c:otherwise>
									<c:out value="${ message.sender.firstname}" />
									<c:out value="${ message.sender.lastname}" />
								</c:otherwise>
							</c:choose>
						</td>
						<td>
							<a href="/EtuJob/message?s=${ message.sender.id}&r=${ message.receiver.id }&mt=${ message.messageTitle}" style="text-decoration:none;">
								<c:out value="${ message.messageTitle}" />
								<c:if test="${ message.sender.id eq Integer.valueOf(connectedId) }">
									<i class="fa fa-mail-forward"></i>
								</c:if>
							</a>
						</td>
						<td>
							<c:out value="${ message.messageDateTime }" />
						</td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>
	<!-- Page footer -->
	<%@ include file="footer.jsp" %>
</body>
</html>