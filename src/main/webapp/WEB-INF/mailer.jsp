<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
</head>
<body>
<div class="w3-yellow">
			<form class="w3-col m7 w3-margin" action="message" method="POST">
				<div class="w3-col m6">
					<h5 class="w3-row">new message</h5>
					<input type="hidden" value="<c:out value="${ senderId}"/>" name="s"/>
					<input type="hidden" value="<c:out value="${ receiverId}"/>" name="r"/>
					<input class="w3-margin" type="text" name="reply_title" placeholder="Titre de mon message"/>
					<textarea class="w3-input w3-light-gray w3-border-0 w3-margin" rows="4" cols="20" id="reply" name="reply"></textarea>
					<input class="w3-button w3-purple" style="margin-left:250px; border-color:purple" type="submit" value="Send &#x27a4;"/>
				</div>		
			</form>
</div>
</body>
</html>