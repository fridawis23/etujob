<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Commentaire</title>
</head>
<body>
	<!-- Header -->
<%@ include file="header.jsp" %>
<%@ include file="menu.jsp" %>
<%
		if(session.getAttribute("firstname")==null){
			response.sendRedirect(request.getContextPath() + "/login");
		}
%>

<div class="w3-container">

		<c:forEach items="${ comment_list }" var="comment" varStatus="status">
			<div class="w3-row m4 w3-light-gray" style="margin-left:30px; margin-bottom:3px">
				<p><c:out value="${ comment.student.firstname}" /> <p>
				<p ><c:out value="${ comment.comment_content}" /></p>
				<p><c:out value="${ comment.comment_date_time}" /></p>
			</div>
		</c:forEach>
		
		
		<form class="w3-col m7 w3-margin" action="comment" method="POST">
				<div class="w3-col m6">
					<h5 class="w3-row">new comment</h5>
					<input type="hidden" value="<c:out value="${ userId}"/>" name="id"/>
					<input type="hidden" value="<c:out value="${service_id}"/>" name="s"/>
					<textarea class="w3-input w3-light-gray w3-border-0 w3-margin" rows="4" cols="20" id="content" name="content"></textarea>
					<input class="w3-button w3-purple" style="margin-left:250px; border-color:purple" type="submit" value="Add &#x27a4;"/>
			       
			
				</div>		
   </form>

</div>




</body>
</html>