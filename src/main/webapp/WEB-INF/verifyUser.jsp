<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
	if(session.getAttribute("firstname")==null ){
		response.sendRedirect(request.getContextPath() + "/login");
	}
	/*else if(!session.getAttribute("connectedStatus").toString().equals("3")){
		response.sendRedirect(request.getContextPath() + "/profile");
	}*/
%>
<!DOCTYPE html>
<!-- user test login: sed.pede@protonmail.ca mdp: OEC83PVW5KK -->
<html>
<head>
<meta charset="UTF-8">
<title>Verify Account</title>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"/>
</head>
<body>
	<%@ include file="header.jsp" %>
	<%@ include file="menu.jsp" %>
	
	<form class="w3-input" action="verifyUser" method="POST">
		<h3>Verify acccount</h3>
		<p class="w3-text-red">${alertmessage }</p>
		<fieldset>
			<label>Email</label>
			<textarea name="email" cols="60" rows="1" disabled>
				${emailToValidate}
			</textarea>
			<input type="hidden" value="${emailToValidate}" name="email"/>
		</fieldset>
		<fieldset>
			<span>
				<label><b>Vendor(Student)</b></label>
				<c:choose>
					<c:when test="${emailValid eq true }">
						<input type="radio" value="1" name="status" checked/>
					</c:when>
					<c:otherwise>
						<input type="radio" value="1" name="status" disabled/>
						<p class="w3-text-red">you need a valid Student email for this option</p>
					</c:otherwise>
				</c:choose>
			</span>
			<p>With that status the owner of the account will be able to <b>SELL</b> his/her servives
			on our platform. Please make your choice and click "Verify user". A verification link 
			will be sent to your mailbox shortly. Kindly use it to confirm your recent changes !</p>
			<span>
				<label><b>Standard user</b></label>
				<input type="radio" value="2" name="status"/>
			</span>
			<p>With that status the owner of the account will be able to enjoy
			our platform as a customer (STANDARD USER). Please make your choice and click "Verify user" below. A verification link 
			will be sent to your mailbox shortly !</p>
			
		</fieldset>
		<input class="w3-btn w3-purple w3-margin" type="submit" value="Verify user"/>
	</form>
	
	<%@ include file="footer.jsp" %>
</body>
</html>