<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
  <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
  <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"/>
<!--link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"-->
   <meta charset="UTF-8">
<title>Manage Services</title>
</head>
<body style="min-height:100vh;">
<%
		if(session.getAttribute("firstname")==null){
			response.sendRedirect(request.getContextPath() + "/login");
		}
		try{
			String connectedId = session.getAttribute("connectedId").toString();
			if(session.getAttribute("connectedStatus").toString().equals("3")){
				response.sendRedirect(request.getContextPath() + "/home");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	%>
<!-- Header -->
<%@ include file="header.jsp" %>
<%@ include file="menu.jsp" %>
	
	<!-- Body -->
	<div class="w3-container" width="380px">
	<span><h2> Pending Requests </h2></span>
			<table class="w3-table w3-col m6 w3-sand">
				<thead>
					<th>Title</th>
					<th>Short Description</th>
					<th>Provider</th>
					<th>Requested On</th>
					<th>Cancel</th>
				</thead>
				<tbody>
				<c:forEach items="${serviceRequests }" var="requestsItem" varStatus="status">
					<tr>
						<td>${requestsItem.announcement.service.serviceTitle }</td>
						<td>${fn:substring(requestsItem.announcement.service.serviceDescription, 0, 25) }...</td>
						<td>${requestsItem.announcement.service.student.firstname } ${requestsItem.announcement.service.student.lastname }</td>
						<td>${requestsItem.requestDate }</td>
						<jsp:useBean id="now" class="java.util.Date"/>
						<td>
							<form action="refuse_request" method="GET">
								<input type="hidden" name="announcement_id" value="${requestsItem.announcement.announcementId}"/>
								<input type="submit" value="Cancel" class="w3-btn w3-red"/>
							</form>
						</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
			
			<!--table class="w3-table w3-col m6">
				<thead>
					<th>Title</th>
					<th>Short Description</th>
					<th>Provider</th>
					<th>Date of Service</th>
					<th>Cancel / Evaluate</th>
				</thead>
				<tbody>
					<tr>
						<td>t1</td>
						<td>t2</td>
						<td>t3</td>
						<td>t4</td>
						<td>t5</td>
					</tr>
				</tbody>
			</table-->
	</div>
	
<!-- Footer -->
<%@ include file="footer.jsp" %>
</body>
</html>