
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"/>
<meta charset="UTF-8">
<title>Welcome to Login page</title>
</head>

<body>
	<%@ include file="header.jsp" %>
	<div class="w3-center" style="margin:auto;">
		
		<form class="w3-container" method="POST" action="passwordforget">
			<div style="margin-bottom:6px;">
				<h3>Password forgotten</h3>
			</div>
			<p class="w3-text-teal">Please, key in your email and you will receive a link into your mailbox to reset your password</p>
			<p class="w3-text-red"> ${ alertMessage }</p>
			<input style="margin: 4px;" type="email" value="${ loginId }" name="loginId" placeholder="email@etujob.fr" required/>
			<br><input class="w3-button w3-hover-purple" style="margin: 4px; background:#bdcebe" type="submit" value="Reset"/>
    	</form>
	</div>
</body>
</html>