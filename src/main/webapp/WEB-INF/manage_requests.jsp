
  <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 
<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
   <meta charset="UTF-8">
<title>Manage Requests</title>
</head>
<body style="min-height:100vh;">
<!-- Header -->
<%@ include file="header.jsp" %>
<%@ include file="menu.jsp" %>

<%
	if(session.getAttribute("firstname")==null ){
		response.sendRedirect(request.getContextPath() + "/login");
	}
	else if(!session.getAttribute("connectedStatus").toString().equals("1"))
		response.sendRedirect(request.getContextPath() + "/profile");
%>

<div class="w3-container">
	<h2>Manage Requests</h2>
	
	<div class="w3-row w3-col m10" style="margin-left:auto;margin-right:auto;">
		<table class="w3-table">
			<thead>
				<th>Service Title</th>
				<th>Apointment Date</th>
				<th>Accept</th>
				<th>Refuse</th>
			</thead>
			<tbody>
				<c:forEach items="${requests}" var="requestItems" varStatus="status">
					<tr>
						<td>${requestItems.announcement.service.serviceTitle}</td>
						<td><c:out value="${requestItems.announcement.announcement_date}"/></td>
						<td><a href="accept_request?announcement_id=${requestItems.announcement.announcementId}&contractor_id=${requestItems.contractor.id}"><span class=" glyphicon glyphicon-ok"></span></a></td>
						<td><a href="refuse_request?announcement_id=${requestItems.announcement.announcementId}"><span class="glyphicon glyphicon-remove"></span></a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div class="w3-center"> <c:out value="${noRequestMsg}"/> </div>
	</div>
</div>

<!-- footer -->
<%@  include file="footer.jsp" %>
</body>
</html>