<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"/>

			

</head>
<body>

<form action="SearchByCategory" method="POST">
	  
 <div class="w3-dropdown-hover">
    <button class="w3-button w3-white">Filter By Category</button>
    <div class="w3-dropdown-content w3-bar-block w3-border">
     <c:forEach   items="${category_list}" var="itemc" varStatus="status" >
     <div class="w3-container">
      <a href="SearchByCategory?category_id=<c:out value="${itemc.categoryId }"/>" class="w3-bar-item w3-button">${itemc.categoryName }</a>
     </div>
     </c:forEach>
    </div>
  </div>
</form>	
	    		

</body>
</html>