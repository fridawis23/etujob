<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<!-- user test login: sed.pede@protonmail.ca mdp: OEC83PVW5KK -->
<html>
<head>
<meta charset="UTF-8">
<title>Profile</title>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"/>
</head>
<body>
	<%@ include file="header.jsp" %>
	<%@ include file="menu.jsp" %>
	
	<%
		if(session.getAttribute("firstname")==null){
			response.sendRedirect(request.getContextPath() + "/login");
		}
	%>
	<div class="w3-container">
		<div  class="w3-container w3-center">
			<h2 >My account</h2>	
		</div>
		
		<div class="w3-row">
			<p class="w3-text-green"><c:out value="${alertMessage }"/></p>
		</div>
		<!--  <img style="opacity: 0.5" src="${pageContext.request.contextPath}/img/calanques.jpg"/ width="200" height="200">-->	
		<div >	
			<div class="w3-col m4">
				<c:if test= "${UserStatus < 3}">
				<div class="w3-panel">	
					<form class="w3-container" method="GET" action="conversations">
						<button class="w3-btn  w3-block  w3-round-large w3-green">Messages</button>
					</form>
				</div>
				</c:if>
				<c:if test= "${UserStatus == 1}">
					<div class="w3-panel">	
						<form class="w3-container" method="GET" action="manage_services">
							<button class="w3-btn  w3-block w3-round-large w3-green">Manage Services</button>
						</form>
					</div>
				</c:if>
				<div  class="w3-panel">
					<form class="w3-container" method="GET" action="verifyUser">
						<button class="w3-btn  w3-block w3-round-large w3-green">Verify my account</button>
					</form>
				</div>		
			</div>
			<div class="w3-col m2">
			</div>
		   	<div class="w3-col m6">
				<form class="w3-container w3-card-4 w3-light-grey w3-text-blue w3-margin" method="POST" action="profile">
						<p class="w3-text-red">${emailError}</p>
						<label for="email" >Email</label>
						<div class="w3-row w3-section">	 
							<div class="w3-rest">
								<input class="w3-input w3-border w3-round" type="email" id="email" name="email" value="${UserInfo.email}" required/>
							</div>
						</div>	     	
						<label for="newpassword">Change password</label>
						<div class="w3-row w3-section">
							<div class="w3-col">
							 </div>
							<div class="w3-rest">
								<input class="w3-input w3-border w3-round"  type="password" id="newpassword" name="newpassword" value=""/>
							</div>
						</div>	
						<input class="w3-input w3-border w3-round" type="hidden" id="status" value ="2" name="status"/>
						
						<label for="profile">Profile</label>
						<div class="w3-row w3-section">
							<div class="w3-rest">
								<textarea class="w3-input w3-border w3-round" rows="10" id="profile" name="profile">${UserInfo.profile}</textarea>
							</div>
						</div>
						<label for="phone">Telephone</label>
						<div class="w3-row w3-section">
							<div class="w3-rest">
								<input class="w3-input w3-border w3-round" type="text" id="phone" name="phone"  value="${UserInfo.phone }"/>
							</div>
						</div>
						<!-- 
						<label for="photo">Your photo here</label>
						<input class="w3-input w3-border-0"  type="file" id="photo" name="photo"/>
								 -->
						<p class="w3-text-red">${ wrongPassword }</p>
						<label for="password">Enter your password</label>
						<div class="w3-row w3-section">
							<div class="w3-rest">
								<input class="w3-input w3-border w3-round"  type="password" id="password" name="password" required/>
							</div>
						</div>
						<label for="confirmpassword">Confirm your password</label>
						<div class="w3-row w3-section">
							<div class="w3-rest">
								<input class="w3-input w3-border w3-round"  type="password" id="confirmpassword" name="confirmpassword" required/>
							</div>
						</div>
						<div class="w3-row w3-section">
							<div class="w3-rest">
								<button class="w3-btn  w3-round-large w3-blue-grey">Submit changes</button>
							</div>
						</div>
				</form>
			</div>
		</div>
	</div>	   
<%@ include file="footer.jsp" %>
</body>
</html>