<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"/>
<meta charset="UTF-8">
<title>Registration page !</title>
</head>

<body>
<%@ include file="header.jsp" %>
<div class="w3-container">
	<div class="w3-col m4 w3-light-grey">
		<div class="w3-container">
			<h3>Registration Form</h3>
			<p class="w3-text-red">${ alertMessage }</p>
		</div>
		<form class="w3-container w3-card-4 w3-light-grey w3-text-blue w3-margin" method="POST" action="register">
			
			<label>Lastname</label>
			<input class="w3-input w3-border-0" type="text" value="${ lastname }" required name="lastname" placeholder="lastname"/>
				
			<label>Firstname</label>
			<input class="w3-input w3-border-0" type="text" value="${ firstname }" required name="firstname" placeholder="firstname"/>
			
			<label>City</label>
			<input class="w3-input w3-border-0" type="text" value="${ city }" required name="city" placeholder="City"/>
				
			<label for="email">Email</label>
			<input class="w3-input w3-border-0" type="email" value="${ email }" required name="email" placeholder="madeleine@email.fr"/>
				
			<label for="password1">Password</label>
			<input class="w3-input w3-border-0"  type="password" value="${ password1 }" required name="password1"/>
			
			<label for="password2">Retype Password</label>
			<input class="w3-input w3-border-0"  type="password" value="${ password2 }" required name="password2"/>
			
			<input class="w3-input w3-border-0" type="hidden" id="status" value ="3" name="status"/>
			
			<label for="profile">Profile</label>
			<textarea class="w3-input w3-border-0" rows="10" value="${ profile }" name="profile"> Please describe your profile here 
			your background 
			your skills
			
			past experience
			your promises
			</textarea>
			
			<label for="phone">telephone (optional)</label>
			<input class="w3-input w3-border-0" type="text" value="${ phone }" name="phone"  placeholder="0800 55 55 55"/>
			
			<label for="photo">Your photo here (optional)</label>
			<input class="w3-input w3-border-0"  type="file" value="${ photo }" name="photo"/>
					
			<button class="w3-btn w3-blue-grey w3-margin">Register</button>
		</form>
	</div>
	<div class="w3-col m8">
		<img style="opacity: 0.5" src="${pageContext.request.contextPath}/img/calanques.jpg"/ width="" height="">
	</div>
	
</div>
</body>
</html>