
  <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"/>
<title>Home</title>
</head>
<body style="min-height:100vh;">
<!-- Header -->
<%@ include file="header.jsp" %>
<%@ include file="menu.jsp" %>

<div class="w3-container w3-text-black" style="background:#d5e1df;">

<div class="w3-row"  >
	<p class="w3-text-green"><c:out value="${alertMessage }" /></p>
</div>

<div class="w3-coll m11">
 <%@ include file="search.jsp" %>
</div>

<!-- menu pour selection les categories -->
<%@include file="searchByCategory.jsp" %>

<!-- listing des annonces -->
<div class="w3-col m11"style="margin-left:200px">
				<c:forEach items="${ announcement_list }" var="announcementsItem" varStatus="status">
	    			<div class="w3-col  m5 w3-margin" style="background-color:rgba(76, 175, 80, 0.10);border-radius: 25px;">
						<div class="w3-container">
			    			<!-- c:out value="${ announcementsItem.announcementId }" / -->
			    			<div class="w3-row" style="margin-left:260px">
			    				<c:out value="${ announcementsItem.announcement_date }" />
			    			</div>
			    			
			    			<h3><c:out value="${ announcementsItem.service.serviceTitle }" /></h3>
			    			<p><c:out value="${ announcementsItem.service.serviceDescription }" /></p>
			    			<h5>proposed by: <c:out value="${ announcementsItem.service.student.firstname}" />
			    			                <c:out value="${ announcementsItem.service.student.lastname}" /></h5>
			    			
			    			<a class="w3-hover-purple" style="margin-left:260px" href="/EtuJob/DetailAnnouncement?id=<c:out value="${ announcementsItem.announcementId }" />">See the details</a>
	    				 
	    				</div>
					</div>
	    		</c:forEach>
	    
	
	<!-- div class="w3-col  m5 w3-margin w3-gray">
		<div class="w3-container">
			<h5>Job 1</h5>
			Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
			Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
			when an unknown printer took a galley of type and scrambled it to make a type 
			specimen book.
		</div>
	</div>
	<div class="w3-col  m5 w3-margin w3-gray">
		<div class="w3-container">
			<h5>Job 2</h5>
			Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
			Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
			when an unknown printer took a galley of type and scrambled it to make a type 
			specimen book.
		</div>
	</div>
	<div class="w3-col  m5 w3-margin w3-gray">
		<div class="w3-container">
			<h5>Job 3</h5>
			Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
			Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
			when an unknown printer took a galley of type and scrambled it to make a type 
			specimen book.
		</div>
	</div>
	<div class="w3-col m5 w3-margin w3-gray">
		<div class="w3-container">
			<h5>Job 4</h5>
			Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
			Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
			when an unknown printer took a galley of type and scrambled it to make a type 
			specimen book.
		</div>
	</div>
	<div class="w3-col m5 w3-margin">
		<h5>Job 5</h5>
		Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
		Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
		when an unknown printer took a galley of type and scrambled it to make a type 
		specimen book.
	</div>
	<div class="w3-col m4 w3-row-padding">
		<h5>Job 6</h5>
		Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
		Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
		when an unknown printer took a galley of type and scrambled it to make a type 
		specimen book.
	</div>
	<div class="w3-col m4 w3-row-padding">
		<h5>Job 7</h5>
		Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
		Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
		when an unknown printer took a galley of type and scrambled it to make a type 
		specimen book.
	</div>
	<div class="w3-col m4 w3-row-padding">
		<h5>Job 8</h5>
		Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
		Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
		when an unknown printer took a galley of type and scrambled it to make a type 
		specimen book.
	</div>
	<div class="w3-col m4 w3-row-padding">
		<h5>Job 9</h5>
		Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
		Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
		when an unknown printer took a galley of type and scrambled it to make a type 
		specimen book.
	</div-->
</div>

</div>

<!-- footer -->
<%@  include file="footer.jsp" %>
</body>
</html>