package com.eservices.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.eservices.entity.Announcement;
import com.eservices.entity.Category;
import com.eservices.entity.Message;
import com.eservices.entity.Request;
import com.eservices.entity.Service;
import com.eservices.entity.Token;
import com.eservices.entity.User;
import com.eservices.entity.Comment;

public class StorageDB implements Storage {

	private Connection connection = null;
	private Statement statement = null;
	
	public StorageDB(String url, String username, String password) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			this.connection = DriverManager.getConnection(url, username, password);
			this.statement = connection.createStatement();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public int addUser(User user) throws Exception{

		PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO users(user_id, user_firstname, user_lastname, user_email, user_password, user_status, user_profil, user_phone, user_photo, user_city) VALUES (?,?,?,?,?,?,?,?,?,?);");
		preparedStatement.setInt(1, 0);
		preparedStatement.setString(2, user.getFirstname());
        preparedStatement.setString(3, user.getLastname());
        preparedStatement.setString(4, user.getEmail());
        preparedStatement.setString(5, user.getPassword());
        preparedStatement.setInt(6, user.getStatus());
        preparedStatement.setString(7, user.getProfile());
        preparedStatement.setString(8, user.getPhone());
        preparedStatement.setString(9, user.getPhoto());
        preparedStatement.setString(10, user.getCity());
        
        int result = preparedStatement.executeUpdate();
        return result;
		//this.statement.executeUpdate("INSERT INTO users VALUES (3, 'Philemon', 'St Jean', 'philemon3006@yahoo.fr', '12345', '2', 'etudiant L3 info avec experience en logistique', '0627178055', 'photo.jpg');");
	}

	@Override
	public int deleteUser() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateUserStatus(String userId, int statusCode) throws Exception{
		PreparedStatement preparedStatement = connection.prepareStatement("UPDATE users SET user_status = ? WHERE user_id = ?");
		preparedStatement.setInt(1, statusCode);
		preparedStatement.setInt(2, Integer.valueOf(userId));
        
        int result = preparedStatement.executeUpdate();
        return result;
	}
	
	@Override
	public int updateUser(User user) throws Exception{
		int result = 0;
		
		User oldUser = getUserById(Integer.toString(user.getId()));
		
		PreparedStatement preparedStatement = connection.prepareStatement("UPDATE users SET user_email = ?,"
				+ "user_profil = ?,"
				+ "user_phone = ?,"
				+ "user_password = ?"
				+ "WHERE user_id = ?");
		
		if(!user.getEmail().equals(""))	
			preparedStatement.setString(1, user.getEmail());
		else
			preparedStatement.setString(1, oldUser.getEmail());
		
		preparedStatement.setString(2, user.getProfile());
		preparedStatement.setString(3, user.getPhone());
		
		if(!user.getPassword().equals(""))	
			preparedStatement.setString(4, user.getPassword());
		else
			preparedStatement.setString(4, oldUser.getPassword());
		
		preparedStatement.setString(5, Integer.toString(user.getId()));
		
		result = preparedStatement.executeUpdate();

		return result;
	}

	

	@Override
	public int updateUserPhone() {
		// TODO Auto-generated method stub
		return 0;
	}

	public User getUserById(String userId) throws Exception {
		User user = new User();
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from users "
				+ "WHERE user_id = ?;");
		preparedStatement.setString(1, userId);
		ResultSet result = preparedStatement.executeQuery();
		
		while(result.next()) {
			
			user.setId(result.getInt("user_id"));
			user.setFirstname(result.getString("user_firstname"));
			user.setLastname(result.getString("user_lastname"));
			user.setStatus(result.getInt("user_status"));
			user.setProfile(result.getString("user_profil"));
			user.setEmail(result.getString("user_email"));
			user.setPassword(result.getString("user_password"));
			user.setPhone(result.getString("user_phone"));
			user.setPhoto(result.getString("user_photo"));
			user.setCity(result.getString("user_city"));
		}
		return user;
	}

	public User getUserByEmail(String loginEmail) throws Exception {
		User user = new User();
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from users "
				+ "WHERE user_email = ?;");
		preparedStatement.setString(1, loginEmail);
		ResultSet result = preparedStatement.executeQuery();
		
		while(result.next()) {
			
			user.setId(result.getInt("user_id"));
			user.setFirstname(result.getString("user_firstname"));
			user.setLastname(result.getString("user_lastname"));
			user.setStatus(result.getInt("user_status"));
			user.setProfile(result.getString("user_profil"));
			user.setEmail(result.getString("user_email"));
			user.setPassword(result.getString("user_password"));
			user.setPhone(result.getString("user_phone"));
			user.setPhoto(result.getString("user_photo"));
			user.setCity(result.getString("user_city"));
		}
		return user;
	}
	@Override
	public List<Announcement> getAnnouncements() throws Exception{
		List<Announcement> announcements = new ArrayList();
		
		// retourner toutes les annonces qui sont actif
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from announcement "
				+ "WHERE announcement_id NOT IN (SELECT announcement_id FROM service_request) AND contractor_id = 0 "+
				"AND announcement_date_time > NOW()"); 
		ResultSet result = preparedStatement.executeQuery();
		
		while(result.next()) {
			Announcement announcementsItem = new Announcement();
			announcementsItem.setAnnouncementId(result.getInt("announcement_id"));
			announcementsItem.setContractor(getUserById(result.getString("contractor_id"))); // objet User a recuperer avec getUser
			announcementsItem.setService(getService(result.getInt("service_id")));; // objet Service a recuperer avec getService
			announcementsItem.setAnnouncement_date(result.getString("announcement_date_time"));
			announcementsItem.setService_price(result.getDouble("service_price"));
			announcementsItem.setPublished_date(result.getString("announcement_published_date"));
			announcements.add(announcementsItem);
		}
		return announcements;
	}
	
	public List<Announcement> getAnnouncementByKeyWord(String keyword) throws Exception {
		List<Announcement> announcements = new ArrayList();
		
		// retourner toutes les annonces qui sont actif
				PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM "
						+ "announcement WHERE contractor_id = 0 AND announcement_date_time > NOW() AND announcement_id NOT IN(SELECT announcement_id FROM service_request) AND service_id IN(SELECT service_id "
						+ "from service where service_description LIKE ? OR service_title LIKE ?)"); 
				preparedStatement.setString(1, keyword);
				preparedStatement.setString(2, keyword);
				ResultSet result = preparedStatement.executeQuery();
				
		while(result.next()) {
			Announcement announcementsItem = new Announcement();
			announcementsItem.setAnnouncementId(result.getInt("announcement_id"));
			announcementsItem.setContractor(getUserById(result.getString("contractor_id"))); // objet User a recuperer avec getUser
			announcementsItem.setService(getService(result.getInt("service_id")));; // objet Service a recuperer avec getService
			announcementsItem.setAnnouncement_date(result.getString("announcement_date_time"));
			announcementsItem.setService_price(result.getDouble("service_price"));
			announcementsItem.setPublished_date(result.getString("announcement_published_date"));
			announcements.add(announcementsItem);
		}
		return announcements;
	}
	
	@Override
	public Service getService(int serviceId) throws Exception {
		
		Service service = new Service();
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from service "
				+ "WHERE service_id = ?;");
		preparedStatement.setInt(1, serviceId);
		ResultSet result = preparedStatement.executeQuery();
		
		while(result.next()) {
			
			service.setServiceId(result.getInt("service_id"));
			service.setServiceTitle(result.getString("service_title"));
			service.setServiceDescription(result.getString("service_description"));
			service.setServiceDuration(result.getString("service_duration"));
			service.setStudent(getUserById(result.getString("user_id")));
			service.setServiceCategory(getCategory(result.getInt("service_category")));
			
		}
		return service;
	}
	
	@Override
	public List<Service>  getServicesByPublisher(int loginId) throws Exception {
		ArrayList<Service> services = new ArrayList();
		
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from service "
				+ "WHERE user_id = ?;");
		preparedStatement.setInt(1, loginId);
		ResultSet result = preparedStatement.executeQuery();
		
		while(result.next()) {
			Service service = new Service();
			service.setServiceId(result.getInt("service_id"));
			service.setServiceTitle(result.getString("service_title"));
			service.setServiceDescription(result.getString("service_description"));
			service.setServiceDuration(result.getString("service_duration"));
			service.setStudent(getUserById(result.getString("user_id")));
			service.setServiceCategory(getCategory(result.getInt("service_category")));
			services.add(service);
		}
		return services;
	}

	@Override
	public Category getCategory(int categoryId) throws Exception {
		Category category = new Category();
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from categories WHERE category_id = ?;");
		preparedStatement.setInt(1, categoryId);
		ResultSet result = preparedStatement.executeQuery();
		
		while(result.next()) {
			
			category.setCategoryId(result.getInt("category_id"));
			category.setCategoryName(result.getString("category_name"));
		}
		return category;
	}

	public List<Message> getLastMessageByConversation(String receiver) throws Exception{
		List<Message> lastMessages = new ArrayList();
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM message WHERE message_date_time "
				+ "IN (SELECT MAX(message_date_time) FROM message WHERE sender_id = ? OR receiver_id = ? GROUP BY message_title) "
				+ "ORDER BY message_date_time DESC");
		preparedStatement.setString(1, receiver);
		preparedStatement.setString(2, receiver);
		ResultSet result = preparedStatement.executeQuery();
		
		while(result.next()) {
			Message msg = new Message();
			msg.setMessageId(result.getInt("message_id"));
			msg.setMessageDateTime(result.getString("message_date_time"));
			msg.setMessageContent(result.getString("message_content"));
			msg.setMessageTitle(result.getString("message_title"));
			msg.setReceiver(getUserById(result.getString("receiver_id")));
			msg.setSender(getUserById(result.getString("sender_id")));	
			lastMessages.add(msg);
		}
		return lastMessages;
	}

	public int getTotalUnreadMessage(String receiver) throws Exception{
		// on ajoute un champs read (0/1) dans la table message
		return 0;
	}

	public List<Message> getDiscussionLines(String senderId, String receiverId, String msgTitle) throws Exception{
		List<Message> messagesDiscussion = new ArrayList();
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM message WHERE "
				+ "receiver_id = ? AND sender_id = ? AND message_title = ? OR sender_id = ? AND "
				+ "receiver_id = ? AND message_title = ? ORDER BY message_date_time ASC");

		preparedStatement.setString(1, receiverId);
		preparedStatement.setString(2, senderId);
		preparedStatement.setString(3, msgTitle);
		preparedStatement.setString(4, receiverId);
		preparedStatement.setString(5, senderId);
		preparedStatement.setString(6, msgTitle);
		
		ResultSet result = preparedStatement.executeQuery();
		
		while(result.next()) {
			Message msg = new Message();
			msg.setMessageId(result.getInt("message_id"));
			msg.setMessageDateTime(result.getString("message_date_time"));
			msg.setMessageContent(result.getString("message_content"));
			msg.setMessageTitle(result.getString("message_title"));
			msg.setReceiver(getUserById(result.getString("receiver_id")));
			msg.setSender(getUserById(result.getString("sender_id")));	
			messagesDiscussion.add(msg);
		}
		return messagesDiscussion;
	}

	public int addMessage(String senderId, String receiverId, String newMessage, String newMessageTitle) throws Exception{
	
		PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO message(sender_id, "
				+ "receiver_id, message_date_time, message_content, message_title) VALUES(?,?,NOW(),?,?)");
		preparedStatement.setInt(1, Integer.valueOf(senderId));
		preparedStatement.setInt(2, Integer.valueOf(receiverId));
		preparedStatement.setString(3, newMessage);
		preparedStatement.setString(4, newMessageTitle);
		
		int result = preparedStatement.executeUpdate();
		return result;
	}

	public int addPasswordResetRequest(String loginId, String uniqueCodegeneratedHash, String deadline) throws Exception{
		PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO password_request"
				+ "(user_id, reset_unique_code, reset_deadline) VALUES "
				+ "(?,?,NOW() + INTERVAL 1 DAY) "
				+ "ON DUPLICATE KEY UPDATE reset_unique_code = ?, reset_deadline = ?");
		preparedStatement.setInt(1, getUserByEmail(loginId).getId());
		preparedStatement.setString(2, uniqueCodegeneratedHash);
		preparedStatement.setString(3, uniqueCodegeneratedHash);
		preparedStatement.setString(4, deadline);
		
        int result = preparedStatement.executeUpdate();
        return result;
	}
	public int updatePasswordResetRequest(String loginId, String uniqueCodegeneratedHash, String deadline) throws Exception{
		PreparedStatement preparedStatement = connection.prepareStatement("UPDATE password_request SET reset_deadline = ? WHERE user_id = ?");
		preparedStatement.setString(1, deadline);
		preparedStatement.setString(2, loginId);
		
        int result = preparedStatement.executeUpdate();
        return result;
	}

	public String getPasswordResetRequest(String loginId) throws Exception{
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT reset_unique_code FROM password_request WHERE user_id = ? AND reset_deadline > NOW()");
		preparedStatement.setInt(1, Integer.valueOf(loginId));
		String uniqueCode = "";
        ResultSet result = preparedStatement.executeQuery();
        while(result.next()) {
        	uniqueCode = result.getString("reset_unique_code");
        }
        return uniqueCode;
	}

	public int updateUserPassword(String loginId, String password) throws Exception{
		PreparedStatement preparedStatement = connection.prepareStatement("UPDATE users SET user_password = ? WHERE user_id = ?");
		preparedStatement.setString(1, password);
		preparedStatement.setInt(2, Integer.valueOf(loginId));
        
        int result = preparedStatement.executeUpdate();
        return result;
	}

	public int addEmailValidationRequest(int userId, String uniqueCodegeneratedHash, String validationDate, String tokenStatus) throws Exception{
		PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO validation_token"
				+ "(user_id, validation_unique_code, validation_date, token_action) VALUES(?,?, ?,?) ON DUPLICATE KEY UPDATE "
				+ "validation_unique_code = ?, validation_date= ?, token_action = ?");
		preparedStatement.setInt(1, userId);
		preparedStatement.setString(2, uniqueCodegeneratedHash);
		preparedStatement.setString(3, validationDate);
		preparedStatement.setString(4, tokenStatus);
		preparedStatement.setString(5, uniqueCodegeneratedHash);
		preparedStatement.setString(6, validationDate);
		preparedStatement.setString(7, tokenStatus);
        int result = preparedStatement.executeUpdate();
        return result;	
	}

	public int setValidationDate(String loginId, String date) throws Exception{
		PreparedStatement preparedStatement = connection.prepareStatement("UPDATE validation_token SET validation_date = ? WHERE user_id = ?");
		preparedStatement.setString(1, date);
		preparedStatement.setInt(2, Integer.valueOf(loginId));
        
        int result = preparedStatement.executeUpdate();
        return result;
	}

	public Token getValidationToken(String loginId) throws Exception{
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM validation_token WHERE user_id = ? AND validation_date = ?");
		preparedStatement.setInt(1, Integer.valueOf(loginId));
		preparedStatement.setString(2, "1900-01-01 00:00:00");
		Token token = new Token();
        ResultSet result = preparedStatement.executeQuery();
        while(result.next()) {
        	 token.setUniqueCode(result.getString("validation_unique_code"));
        	 token.setValidationDate(new SimpleDateFormat("yyyy-mm-dd").parse(result.getString("validation_date")));
        	 token.setAccessLevel(Integer.valueOf(result.getString("token_action")));
        }
        return token;
	}

	public int addAnnouncement(Service service, String announcementDate, User contractor, double price) throws Exception{
		PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO announcement(service_id, contractor_id, "
				+ "announcement_date_time, service_price, announcement_published_date) "
				+ "VALUES (?,?,?,?, NOW())");
		preparedStatement.setInt(1, service.getServiceId());
		preparedStatement.setInt(2, contractor.getId());
		preparedStatement.setString(3, announcementDate);
		preparedStatement.setDouble(4, price);
		
        int result = preparedStatement.executeUpdate();
        return result;
	}
	
	@Override
	public List<Announcement> getAnnouncementByCategoryId(int categoryId) throws Exception{
         List<Announcement> announcements = new ArrayList();
		
		// retourner toutes les annonces qui sont actif
				PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM "
						+ "announcement WHERE contractor_id = 0 AND announcement_date_time > NOW() AND announcement_id NOT IN (SELECT announcement_id from service_request) AND service_id IN(SELECT service_id "
						+ "from service WHERE service_category=?); ");
				
				preparedStatement.setInt(1, categoryId);
				ResultSet result = preparedStatement.executeQuery();
				
		while(result.next()) {
			Announcement announcementsItem = new Announcement();
			announcementsItem.setAnnouncementId(result.getInt("announcement_id"));
			announcementsItem.setContractor(getUserById(result.getString("contractor_id"))); // objet User a recuperer avec getUser
			announcementsItem.setService(getService(result.getInt("service_id")));; // objet Service a recuperer avec getService
			announcementsItem.setAnnouncement_date(result.getString("announcement_date_time"));
			announcementsItem.setService_price(result.getDouble("service_price"));
			announcementsItem.setPublished_date(result.getString("announcement_published_date"));
			announcements.add(announcementsItem);
		}
		return announcements;
	}
	
	@Override
	public Announcement getAnnouncementById (int announcementId) throws Exception{
		Announcement announcementsItem = new Announcement();
		
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from announcement WHERE announcement_id= ?;"); 
	
		preparedStatement.setInt(1, announcementId);
		ResultSet result=preparedStatement.executeQuery();
		
		while(result.next()) {
			
			announcementsItem.setAnnouncementId(result.getInt("announcement_id"));
			announcementsItem.setContractor(getUserById(result.getString("contractor_id"))); // objet User a recuperer avec getUser
			announcementsItem.setService(getService(result.getInt("service_id")));; // objet Service a recuperer avec getService
			announcementsItem.setAnnouncement_date(result.getString("announcement_date_time"));
			announcementsItem.setService_price(result.getDouble("service_price"));
			announcementsItem.setPublished_date(result.getString("announcement_published_date"));
			
			
			
		}
		return announcementsItem;
	}
	@Override
	public List<Category> getAllCategory() throws Exception {
		
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from categories");
		ResultSet result = preparedStatement.executeQuery();
		
		List<Category> listCategory = new ArrayList<>();
		
		while(result.next()) {
			Category category = new Category();
			category.setCategoryId(result.getInt("category_id"));
			category.setCategoryName(result.getString("category_name"));
			
			listCategory.add(category);
		}
		return listCategory;
	}
	
	@Override
	public int createService(Service service) throws SQLException {
		PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO service(service_category, user_id, service_title, service_description, service_duration) VALUES (?, ?, ?, ?, ?);");
		preparedStatement.setInt(1, service.getServiceCategory().getCategoryId());
		preparedStatement.setInt(2, service.getStudent().getId());
		preparedStatement.setString(3, service.getServiceTitle());
		preparedStatement.setString(4,service.getServiceDescription());
		preparedStatement.setString(5,service.getServiceDuration());

		
		int result = preparedStatement.executeUpdate();
		return result;
	}

	public int addServiceRequest(String announcementId, String contractorId, String requestNote, String requestDate,
			String validationDate) throws Exception{
		PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO service_request"
				+ "(service_request_contractor_id, service_request_note, service_request_date, service_validation_date, announcement_id)"
				+ "values(?,?,?,?,? )");
		preparedStatement.setInt(1, Integer.valueOf(contractorId));
		preparedStatement.setString(2, requestNote);
		preparedStatement.setString(3, requestDate);
		preparedStatement.setString(4,validationDate);
		preparedStatement.setInt(5,Integer.valueOf(announcementId));
		
		int result = preparedStatement.executeUpdate();
		return result;
	}

	public List<Request> getRequestByContractor(String userId) throws Exception{
		
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from service_request WHERE service_request_contractor_id = ?");
		preparedStatement.setString(1, userId);
		ResultSet result = preparedStatement.executeQuery();
		
		List<Request> requestList = new ArrayList<>();
		
		while(result.next()) {
			Request serviceRequest = new Request();
			serviceRequest.setAnnouncement(getAnnouncementById(result.getInt("announcement_id")));
			serviceRequest.setContractor(null);
			serviceRequest.setRequestNote(result.getString("service_request_note"));
			;
			serviceRequest.setRequestDate(new SimpleDateFormat("yyyy-mm-dd").parse(result.getString("service_request_date")));
			serviceRequest.setValidationDate(new SimpleDateFormat("yyyy-mm-dd").parse(result.getString("service_validation_date")));
			requestList.add(serviceRequest);
		}
		
		return requestList;
	}
	
	public List<Comment> getCommentLines(String serviceId) throws Exception{
		List<Comment> comments=new ArrayList();
		
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM comment "
				+ "WHERE service_id = ? ORDER BY comment_date_time ASC ");
		
		preparedStatement.setString(1, serviceId);
		
		ResultSet result = preparedStatement.executeQuery();
		
        while(result.next()) {
        	Comment comment=new Comment();
        	comment.setCommentId(result.getInt("comment_id"));
        	comment.setService(getService(result.getInt("service_id")));
        	comment.setStudent(getUserById(result.getString("user_id")));
        	comment.setComment_date_time(result.getString("comment_date_time"));
        	comment.setComment_date_time(result.getString("comment_content"));
        	comments.add(comment);	
			
		}
		return comments;
	}
	
	public  int addComment(String serviceId, String userId, String content)  throws Exception{
		
		PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO comment(service_id,"
				+"user_id, comment_date_time, comment_content) VALUES(?,?,NOW(),?)");
		preparedStatement.setInt(1, Integer.valueOf(serviceId));
		preparedStatement.setInt(2, Integer.valueOf(userId));
		preparedStatement.setString(3, content);
		
		int result = preparedStatement.executeUpdate();
		return result;
	}

	@Override
	public List<Category> getCategory() throws Exception{
		List<Category> category = new ArrayList();
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from categories ;");
	
		ResultSet result = preparedStatement.executeQuery();
		
		while(result.next()) {
			Category categoryItem=new Category();
			categoryItem.setCategoryId(result.getInt("category_id"));
			categoryItem.setCategoryName(result.getString("category_name"));
			category.add(categoryItem);
		}
		return category;
	}
	
	@Override
	public int updateRequestValidation(int announcement_id) throws SQLException {
		PreparedStatement preparedStatement = connection.prepareStatement("UPDATE service_request SET service_validation_date = CURRENT_TIMESTAMP WHERE announcement_id = ?");
		preparedStatement.setInt(1, announcement_id);
        
        int result = preparedStatement.executeUpdate();
        return result;	
	}
	
	@Override
	public int acceptRequest(int announcement_id, int contractor_id) throws SQLException {
		//updateRequestValidation(announcement_id);
		PreparedStatement preparedStatement = connection.prepareStatement("UPDATE announcement SET contractor_id = ? WHERE announcement_id = ?");
		preparedStatement.setInt(1, contractor_id);
		preparedStatement.setInt(2, announcement_id);
        
        int result = preparedStatement.executeUpdate();
        return result;	
	}
	
	@Override
	public int deleteRequest(int announcement_id) throws SQLException {
		PreparedStatement preparedStatement = connection.prepareStatement("Delete from service_request WHERE announcement_id = ?");
		preparedStatement.setInt(1, announcement_id);
        
        int result = preparedStatement.executeUpdate();
        return result;	
	}
	
	@Override
	public int deleteService(int service) throws SQLException {
		PreparedStatement preparedStatement = connection.prepareStatement("Delete from service where service_id = ?;");
		preparedStatement.setInt(1, service);
		int result = preparedStatement.executeUpdate();
		return result;
	}
	
	@Override
	public List<Request> getServiceRequest(int user_id) throws Exception {
		StringBuilder parameters = new StringBuilder();
		
		PreparedStatement preparedStatementID = connection.prepareStatement("Select announcement_id "
					+ "from announcement join service on announcement.service_id = service.service_id "
					+ "WHERE user_id = ?");
		preparedStatementID.setInt(1, user_id);
		
		ResultSet resultId = preparedStatementID.executeQuery();
		
		while(resultId.next()) {
			
				parameters.append("announcement_id = ");
				parameters.append(resultId.getInt("announcement_id"));
			
			if(!resultId.isLast())
				parameters.append(" OR ");
		}
		
		if(parameters.toString().equals("")) 
			return null;
		
		
		//GET Service request
		List<Request> requests = new ArrayList<>();
		PreparedStatement preparedStatementRequest = connection.prepareStatement("SELECT * from service_request"
				+ " Where " + parameters + ";");// join service on announcement.service_id = service.service_id"
				//+ "WHERE user_id = ?);");
				 //"Where announcement_id IN ("+ parameters.toString() + ");");
		//preparedStatementRequest.setInt(1, user_id);
		ResultSet result = preparedStatementRequest.executeQuery();
		
		while(result.next()) {
			Request request = new Request();
			request.setAnnouncement(getAnnouncementById(result.getInt("announcement_id")));
			request.setContractor(getUserById(Integer.toString(result.getInt("service_request_contractor_id"))));
			request.setRequestDate(new SimpleDateFormat("yyyy-mm-dd").parse(result.getString("service_request_date")));
			request.setRequestNote(null);
			
			requests.add(request);
		
		}
	//}
		return requests;
	}

	@Override
	public int modifyService(Service service) throws Exception {
		Service oldService = getService(service.getServiceId());
		PreparedStatement preparedStatement = connection.prepareStatement("UPDATE service SET "
				+ "service_title = ?,"
				+ "service_description = ?,"
				+ "service_duration = ?,"
				+ "service_category = ?"
				+ " WHERE service_id = ?");
		
		
		preparedStatement.setString(1,service.getServiceTitle()); 
		preparedStatement.setString(2,service.getServiceDescription());
		preparedStatement.setString(3,service.getServiceDuration());
		preparedStatement.setInt(4,service.getServiceCategory().getCategoryId());
		preparedStatement.setInt(5,service.getServiceId());
		
        int result = preparedStatement.executeUpdate();
        return result;	
		
	}
}
