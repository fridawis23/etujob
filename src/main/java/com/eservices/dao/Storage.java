package com.eservices.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.eservices.entity.Announcement;
import com.eservices.entity.Service;
import com.eservices.entity.User;
import com.eservices.entity.Category;
import com.eservices.entity.Comment;
import com.eservices.entity.Message;
import com.eservices.entity.Request;

public interface Storage {

	public int addUser(User user) throws Exception;
	public User getUserById(String email) throws Exception;
	public int updateUser(User user) throws Exception;
	public int deleteUser();
	public int updateUserStatus(String userId, int statusCode) throws Exception;
	public int updateUserPhone();
	
	public List<Announcement> getAnnouncements() throws Exception;
	public List<Announcement> getAnnouncementByKeyWord(String keyword) throws Exception;
	public List<Announcement> getAnnouncementByCategoryId(int categoryId) throws Exception;
	public Announcement getAnnouncementById (int announcementId) throws Exception;
	public List<Message> getLastMessageByConversation(String receiver) throws Exception;
	public List<Message> getDiscussionLines(String senderId, String receiverId, String msgTitle) throws Exception;
	public int addMessage(String senderId, String receiverId, String newMessage, String newMessageTitle) throws Exception;
	public Service getService(int serviceId) throws Exception;
	public List<Service>  getServicesByPublisher(int LoginId) throws Exception;
	//public int addAnnouncement(Announcement announcement);
	///public int addService(Service service);
	public Category getCategory(int categoryId) throws Exception;
	public List<Category> getCategory() throws Exception;
	public int createService(Service service) throws SQLException;
	public List<Category> getAllCategory() throws Exception;
	public List<Comment> getCommentLines(String serviceId) throws Exception;
	public  int addComment(String serviceId, String userId, String content) throws Exception;
	
	public int updateRequestValidation(int announcement_id) throws SQLException;
	public int acceptRequest(int announcement_id, int contractor_id) throws SQLException;
	public int deleteService(int service) throws SQLException;
	public List<Request> getServiceRequest(int user_id) throws Exception;
	int deleteRequest(int announcement_id) throws SQLException;
	int modifyService(Service service) throws Exception;
}
