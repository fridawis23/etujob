package com.eservices.helper;

import java.util.Random;

public class UniqueCode {
	
	public static String generate(int length) {
		String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
		StringBuilder builder = new StringBuilder();
		Random random = new Random();
		for(int i = 0; i < length; i++) {
			int index = random.nextInt(alphabet.length());
			builder.append(alphabet.charAt(index));
		}
		return builder.toString();
	}
}
