package com.eservices.helper;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/*
 * Classe inspiree du tutoriel "envoyer un email avec Java et Gmail" de Waytolearnx.com
 * ref: https://waytolearnx.com/2020/03/envoyer-un-mail-avec-java-en-utilisant
 * -gmail.html#:~:text=L'envoi%20d'e%2D,lui%20transmettre%20les%20param%C3%A8tres%20requis.
 * 
 * publie: 20 mars 2020, consulte : 16 mai 2020
 * */
public class MailingHandler {
	public static void sendEmail(String to, String subject, String msgBody) {
		final String from = "admin@etujob.live";
		final String passwd = "algo2022@Etujob";
		
		Properties prop = new Properties();
		prop.put("mail.smtp.host", "smtp.titan.email");
		prop.put("mail.smtp.socketFactory.port", "465");
		prop.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		prop.put("mail.smtp.auth","true");
		prop.put("mail.smtp.port", "465");
	//	prop.put("mail.smtp.starttls.enable", "true");
		 
		Session sessionMail = Session.getDefaultInstance(prop, 
				new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(from, passwd);
			}
		});	
	
		try {
			MimeMessage message = new MimeMessage(sessionMail);
			message.setFrom(new InternetAddress(from));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.setSubject(subject);
			message.setText(msgBody);
			Transport.send(message);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}





























