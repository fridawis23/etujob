package com.eservices.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.eservices.dao.StorageDB;
import com.eservices.entity.Announcement;
import com.eservices.entity.Category;
import com.eservices.entity.Service;

/**
 * Servlet implementation class SearchByCategoryServlet
 */
@WebServlet("/SearchByCategory")
public class SearchByCategoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchByCategoryServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("category_id");
		StorageDB mysql_server = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
		List<Category> category_list=null;
		List<Announcement> announcement_list = null;
		try {
			
			announcement_list = mysql_server.getAnnouncementByCategoryId(Integer.parseInt(id));
			category_list=mysql_server.getCategory();
		
	   } catch (Exception e) {
		e.printStackTrace();
	   }
		
	    request.setAttribute("announcement_list", announcement_list);
	    request.setAttribute("category_list", category_list);
	    request.setAttribute("id", id);
				
		this.getServletContext().getRequestDispatcher("/WEB-INF/home.jsp").forward(request, response);
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		doGet(request, response);
	}

}
