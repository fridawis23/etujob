package com.eservices.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.eservices.dao.StorageDB;
import com.eservices.entity.Announcement;
import com.eservices.entity.User;

/**
 * Servlet implementation class DetailAnnonce
 */
@WebServlet("/ProfilAnnouncement")
public class ProfilAnnouncementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProfilAnnouncementServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		StorageDB mysql_server = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
		User user=new User();
		String id=request.getParameter("id");
		try {
			
			user=mysql_server.getUserById(id);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		request.setAttribute("user", user);
		this.getServletContext().getRequestDispatcher("/WEB-INF/profilAnnouncement.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
