package com.eservices.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.crypto.bcrypt.BCrypt;

import com.eservices.dao.StorageDB;
import com.eservices.entity.User;
import com.eservices.helper.MailingHandler;
import com.eservices.helper.UniqueCode;

import java.util.regex.*;
/**
 * Servlet implementation class VerifyAccountServlet
 */
@WebServlet("/verifyUser")
public class VerifyAccountServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	String alertMessage = "";
    public VerifyAccountServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final Pattern pattern;
		final Matcher matcher;
		Boolean emailValid = false;
		HttpSession session = request.getSession();
		String emailToValidate = session.getAttribute("connectedEmail").toString();
		pattern = Pattern.compile("@etu.univ-amu.fr");
		matcher = pattern.matcher(emailToValidate);
		while(matcher.find()) {
			emailValid = true;
		}
		alertMessage = request.getParameter("message");
		request.setAttribute("emailValid", emailValid);
		request.setAttribute("emailToValidate", emailToValidate);
		this.getServletContext().getRequestDispatcher("/WEB-INF/verifyUser.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email = request.getParameter("email");
		String status = request.getParameter("status");
		
		request.setAttribute("email", email);
		
		// generate token for email validation
					String uniqueCode = UniqueCode.generate(6);
					String uniqueCodegeneratedHash = BCrypt.hashpw(uniqueCode, BCrypt.gensalt(12));
					uniqueCodegeneratedHash += "/";
					User recipient = new User();
					StorageDB mysqlServer = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
					try {
						recipient = mysqlServer.getUserByEmail(email);
						// storing the token in the database
						mysqlServer.addEmailValidationRequest(recipient.getId(), uniqueCodegeneratedHash, "1900-01-01 00:00:00", status); 
					} catch (Exception e) {
						e.printStackTrace();
						alertMessage = "An error occurred !";
						request.setAttribute("alertMessage", alertMessage);
						doGet(request, response);
					}
					
					// build email
					StringBuilder msgContent = new StringBuilder();
					msgContent.append("Dear " + recipient.getFirstname() + " " + recipient.getLastname() + ", "
					+ "\n Your account updates have been recorded.\n" 
					+ "Please use the link below to validate changes and fully enjoy Etu'Job.\n");
					msgContent.append("http://localhost:8080/EtuJob/accountvalidation?id=" + recipient.getId() 
					+ "&c=" + uniqueCodegeneratedHash);
					msgContent.append("\nWith regards\n");
					// send email
					String message = msgContent.toString();
					String sender = recipient.getEmail();
					MailingHandler.sendEmail(sender, " Etu'Job | Validation link", message);
					alertMessage = "An email has been sent to you, please use the link in the email to validate your change";
					request.setAttribute("alertMessage", alertMessage);
					response.sendRedirect(request.getContextPath() + "/profile?a="+alertMessage);
					return;
		
		//doGet(request, response);
	}

}
