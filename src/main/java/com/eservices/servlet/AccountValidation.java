package com.eservices.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.eservices.dao.StorageDB;
import com.eservices.entity.Token;


@WebServlet("/accountvalidation")
public class AccountValidation extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private static final int STANDARD_USER = 2;
    
    public AccountValidation() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String loginId = request.getParameter("id");
		String resetUniqueCode = request.getParameter("c");
		Token token;
		StorageDB mysqlServer = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
		try {
				token = mysqlServer.getValidationToken(loginId);
				
				String storedUniqueCodeHash = token.getUniqueCode();
				
				if(storedUniqueCodeHash == null) { 
					String alertMessage = "This profile is already active";
					request.setAttribute("alertMessage", alertMessage);
					this.getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
					return;
				}
				if (!storedUniqueCodeHash.equals(resetUniqueCode)) {
					String alertMessage = "This profile is already active";
					request.setAttribute("alertMessage", alertMessage);
					this.getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
					return;
				}
				
				SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
				Date validationName = new Date(System.currentTimeMillis());
				mysqlServer.updateUserStatus(loginId, token.getAccessLevel());
				mysqlServer.setValidationDate(loginId, sdf.format(validationName)); 
				
				String alertMessage = "Your profile is now active, please login!";
				request.setAttribute("alertMessage", alertMessage);
				this.getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
		}catch(Exception e) {
			e.printStackTrace();
			
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
