package com.eservices.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.eservices.dao.StorageDB;
import com.eservices.entity.Announcement;
import com.eservices.helper.MailingHandler;

/**
 * Servlet implementation class AcceptRequestServlet
 */
@WebServlet("/accept_request")
public class AcceptRequestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AcceptRequestServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		StorageDB mysql_server = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
		HttpSession session = request.getSession();
		String connectedId = Integer.toString((int) session.getAttribute("connectedId"));
		int announcement_id = Integer.parseInt(request.getParameter("announcement_id"));
		int connectedStatus = (int) session.getAttribute("connectedStatus");
		if(connectedStatus == 1) {
			try {
				//mysql_server.updateRequestValidation(announcement_id);
				
				mysql_server.acceptRequest(announcement_id, Integer.parseInt(request.getParameter("contractor_id")));
				
				mysql_server.addMessage(connectedId, request.getParameter("contractor_id"),"Request accepted" , "Request accepted");
				mysql_server.deleteRequest(announcement_id);
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			response.sendRedirect(request.getContextPath() + "/message?s=" + connectedId + "&r=" + request.getParameter("contractor_id"));
		}
		else
			response.sendRedirect(request.getContextPath() + "/profile");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
