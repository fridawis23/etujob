package com.eservices.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.eservices.dao.StorageDB;
import com.eservices.entity.Announcement;
import com.eservices.entity.Comment;
import com.eservices.entity.User;

/**
 * Servlet implementation class CommentServlet
 */
@WebServlet("/comment")
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CommentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String userId=request.getParameter("id");
		String service_id=request.getParameter("s");
		StorageDB mysql_server = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
		
	
		
		List<Comment> comment_list=null;
		Announcement announcement=null;
		try {
		
			comment_list=mysql_server.getCommentLines(service_id);
			
		} catch (Exception e) {
			e.printStackTrace();
			
		}
	
		request.setAttribute("comment_list",comment_list);
		request.setAttribute("announcement",announcement);
		request.setAttribute("userId", userId);
		request.setAttribute("service_id", service_id);
		this.getServletContext().getRequestDispatcher("/WEB-INF/comment.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String service_id=request.getParameter("s");
		String user_id=request.getParameter("id");
		String content=request.getParameter("content");
		
		
		StorageDB mysql_server = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
		
		
		try {
			
			mysql_server.addComment(service_id,user_id,content);
			
		}catch(Exception e) {
			e.printStackTrace();
		}
	
		doGet(request, response);

	}
	

}
