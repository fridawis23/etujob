package com.eservices.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.crypto.bcrypt.BCrypt;

import com.eservices.dao.StorageDB;
import com.eservices.entity.User;

/**
 * Servlet implementation class Job
 */
@WebServlet("/profile")
public class ProfileServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProfileServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		StorageDB mysql_server = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
		HttpSession session = request.getSession();
		String alertMessage = request.getParameter("a");
		String connectedId = null;
		User currentUser = null;
		try {
			connectedId = session.getAttribute("connectedId").toString();
			currentUser = mysql_server.getUserById(connectedId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(connectedId != null)	
			request.setAttribute("UserStatus", currentUser.getStatus());
		request.setAttribute("UserInfo", currentUser);
		request.setAttribute("alertMessage", alertMessage);
		this.getServletContext().getRequestDispatcher("/WEB-INF/profile.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		StorageDB mysql_server = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
		User user = new User();
		HttpSession session = request.getSession();
		
		//Crypter le mdp
		String passwordToStore = BCrypt.hashpw(request.getParameter("newpassword"), BCrypt.gensalt());
		String wrongPassword = "Invalid password or not matching";
		String emailError = "Invalid email";
		String connectedId = session.getAttribute("connectedId").toString();
		
		user.setId(Integer.valueOf(connectedId));
		user.setEmail(request.getParameter("email"));
		user.setProfile(request.getParameter("profile"));
		user.setPhone(request.getParameter("phone"));
		//si le champs est vide envoie une string vide
		if(request.getParameter("newpassword").equals(""))
			user.setPassword(request.getParameter("newpassword"));
		else
			user.setPassword(passwordToStore);
		
		String password = request.getParameter("password");
		String confirmPassword = request.getParameter("confirmpassword");
		String storedPassword = null;
		try {
			storedPassword = mysql_server.getUserById(connectedId).getPassword();
			if(password.equals(confirmPassword) && BCrypt.checkpw(password,storedPassword))
				mysql_server.updateUser(user);	
			else
				request.setAttribute("wrongPassword", wrongPassword);
				
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			request.setAttribute("emailError", emailError);
		}
		
		doGet(request, response);
	}

}















