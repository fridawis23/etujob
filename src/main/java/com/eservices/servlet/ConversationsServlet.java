package com.eservices.servlet;

import java.io.IOException;
import java.util.List;

import javax.mail.Session;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.eservices.dao.StorageDB;
import com.eservices.entity.Message;

@WebServlet("/conversations")
public class ConversationsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
    public ConversationsServlet() {
        super();
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		StorageDB mysqlServer = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
		List<Message> lastMessageReceived = null;
		int totalUnreadMessage = 0;
		HttpSession session = request.getSession();
		
		try {
			String receiverId = session.getAttribute("connectedId").toString();
			// recupere toutes les annonces de la BDD
			
			lastMessageReceived = mysqlServer.getLastMessageByConversation(receiverId); 
			totalUnreadMessage = mysqlServer.getTotalUnreadMessage("");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// transmet une ArrayList d'annonces a la page d'accueil
		request.setAttribute("messages_list", lastMessageReceived);
		this.getServletContext().getRequestDispatcher("/WEB-INF/conversation.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
