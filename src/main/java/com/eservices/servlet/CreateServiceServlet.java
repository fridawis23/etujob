package com.eservices.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.eservices.dao.StorageDB;
import com.eservices.entity.Category;
import com.eservices.entity.Service;
import com.eservices.entity.User;

/**
 * Servlet implementation class CreateserviceServlet
 */
@WebServlet("/create_service")
public class CreateServiceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateServiceServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		StorageDB mysql_server = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
		List<Category> listCategory = new ArrayList<>();
		
		try {
			listCategory = mysql_server.getAllCategory();
	
			request.setAttribute("categories", listCategory);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/create_service.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		StorageDB mysql_server = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
		HttpSession session = request.getSession();
		String connectedId = session.getAttribute("connectedId").toString();
		Category category = null;
		User student = new User();
		student.setId(Integer.valueOf(connectedId));
		
		try {
			category = mysql_server.getCategory(Integer.parseInt(request.getParameter("serviceCategory")));
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		Service service = new Service();
		service.setServiceTitle(request.getParameter("serviceTitle"));
		service.setServiceDescription(request.getParameter("serviceDescription"));
		service.setServiceDuration(request.getParameter("serviceDuration"));
		service.setServiceCategory(category);
		service.setStudent(student);
		
		try {
			mysql_server.createService(service);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		response.sendRedirect(request.getContextPath() + "/manage_services");
	
	
	}

}
