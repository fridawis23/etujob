package com.eservices.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.eservices.dao.StorageDB;
import com.eservices.entity.Announcement;
import com.eservices.entity.Message;
import com.eservices.helper.MailingHandler;

/**
 * Servlet implementation class Message
 */
@WebServlet("/message")
public class MessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public MessageServlet() {
        super();
      
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String senderId = request.getParameter("s");
		String receiverId = request.getParameter("r");
		String msgTitle = request.getParameter("mt");
		StorageDB mysqlServer = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
		List<Message> messageDiscussion = null;
		int totalUnreadMessage = 0;
		
		try {
			
			messageDiscussion = mysqlServer.getDiscussionLines(senderId, receiverId, msgTitle); 
			//totalUnreadMessage = mysqlServer.getTotalUnreadMessage("");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// transmet une ArrayList d'annonces a la page d'accueil
		request.setAttribute("messages_discussion", messageDiscussion);
		request.setAttribute("receiverId", receiverId);
		request.setAttribute("senderId", senderId);
		request.setAttribute("msgTitle", msgTitle);
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/message.jsp").forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String senderId = request.getParameter("s");
		String receiverId = request.getParameter("r");
		String newMessage = request.getParameter("reply");
		String newMessageTitle = request.getParameter("mt");
		Message message = new Message();
		
		StorageDB mysqlServer = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
		List<Message> messageDiscussion = null;
		int totalUnreadMessage = 0;
		
		try {
			HttpSession session = request.getSession();
			String connectedId = session.getAttribute("connectedId").toString();
			String recipient = receiverId;
			if(connectedId.equals(receiverId)) {
				recipient = senderId;
			}
			if(!newMessage.isBlank()) {
				mysqlServer.addMessage(connectedId, recipient, newMessage, newMessageTitle); 
				//MailingHandler.sendEmail("philemon.stjean@esih.edu", newMessageTitle, newMessage);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		doGet(request, response);
	}
}
