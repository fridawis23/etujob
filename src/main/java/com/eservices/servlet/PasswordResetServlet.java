package com.eservices.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.springframework.security.crypto.bcrypt.BCrypt;

import com.eservices.dao.StorageDB;

@WebServlet("/passwordreset")
public class PasswordResetServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    public PasswordResetServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String loginId = request.getParameter("id");
		String resetUniqueCode = request.getParameter("c");
		request.setAttribute("loginId", loginId);
		request.setAttribute("uniqueCode", resetUniqueCode);
		StorageDB mysql_server = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
		try {
			// assure la validite d'un lien reset
			 String storedResetUniqueCode = mysql_server.getPasswordResetRequest(loginId);
			 String alertMessage = "This link is no longer valid";
			 if(!storedResetUniqueCode.equals(resetUniqueCode)) {
				 request.setAttribute("alertMessage", alertMessage);
				 this.getServletContext().getRequestDispatcher("/WEB-INF/passwordreset.jsp").forward(request, response);	
				 return;
			 }
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.getServletContext().getRequestDispatcher("/WEB-INF/passwordreset.jsp").forward(request, response);	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String loginId = request.getParameter("id");
		String resetUniqueCode = request.getParameter("c");
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");
		StorageDB mysql_server = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
		
		try {
		String storedResetUniqueCode = mysql_server.getPasswordResetRequest(loginId);
		
		// assure la validite d'un lien reset dans le doPost
		 if(!storedResetUniqueCode.equals(resetUniqueCode)) {
			 String alertMessage = "This link is no longer valid!";
			 request.setAttribute("alertMessage", alertMessage);
			 request.setAttribute("loginId", loginId);
			 request.setAttribute("uniqueCode", resetUniqueCode);
			 doGet(request, response);
			 return;
		 }
		 // assure que password1 et password2 sont similaires
		if(!password1.equals(password2)) {
			String alertMessage = "Password Mismatched!";
			 request.setAttribute("alertMessage", alertMessage);
			request.setAttribute("loginId", loginId);
			request.setAttribute("uniqueCode", resetUniqueCode);
			doGet(request, response);
			return;
		}
		
		String password = BCrypt.hashpw(password1, BCrypt.gensalt());
		
			int result = mysql_server.updateUserPassword(loginId, password);
			if (result == 1) {
				SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
				Date deadline = new Date(System.currentTimeMillis());
				mysql_server.updatePasswordResetRequest(loginId, resetUniqueCode, sdf.format(deadline)); 
				String alertMessage = "Your new password was set, please login !";
				response.sendRedirect(request.getContextPath() + "/login?a="+alertMessage);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
