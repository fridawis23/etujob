package com.eservices.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.eservices.dao.StorageDB;
import com.eservices.entity.Request;


@WebServlet("/requests")
public class RequestContentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public RequestContentServlet() {
        super();
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userId = request.getParameter("id");
		StorageDB mysqlServer = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
		List<Request> serviceRequestsPending = new ArrayList<>();
		List<Request> serviceRequestsArchived = new ArrayList<>();
		
		try {
				serviceRequestsPending = mysqlServer.getRequestByContractor(userId);
				//serviceRequestsArchived = mysqlServer.getAnnouncementByContractor(userId);		
		}catch(Exception e) {
			e.printStackTrace();
			
		}
		Date currentTime = new Date(System.currentTimeMillis());
		request.setAttribute("currentTime", currentTime);
		request.setAttribute("serviceRequests", serviceRequestsPending);
		//request.setAttribute("serviceRequestsArchived", serviceRequestsArchived);
		this.getServletContext().getRequestDispatcher("/WEB-INF/myrequestframe.jsp").forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
