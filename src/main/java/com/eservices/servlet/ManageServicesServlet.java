package com.eservices.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.eservices.dao.StorageDB;
import com.eservices.entity.Service;
import com.eservices.entity.User;

/**
 * Servlet implementation class ManageServices
 */
@WebServlet("/manage_services")
public class ManageServicesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ManageServicesServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		StorageDB mysql_server = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
		HttpSession session = request.getSession();
		int connectedId = (int) session.getAttribute("connectedId");
		List<Service> listService = null;
		
		try {
			listService = mysql_server.getServicesByPublisher(connectedId);
			request.setAttribute("services", listService);	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		User user = null;
		try {
			user = mysql_server.getUserById(Integer.toString(connectedId));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(user.getStatus() != 1)
			response.sendRedirect(request.getContextPath() + "/home");
		else
			this.getServletContext().getRequestDispatcher("/WEB-INF/manage_service.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
