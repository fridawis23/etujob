package com.eservices.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.eservices.dao.StorageDB;
import com.eservices.entity.Category;
import com.eservices.entity.Service;
import com.eservices.entity.User;


@WebServlet("/publish")
public class PublishServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
    public PublishServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String announcementId = request.getParameter("id");
		
		StorageDB mysqlServer = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
		Service service = new Service();
		try {
			service = mysqlServer.getService(Integer.valueOf(announcementId));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		request.setAttribute("service",service);
		this.getServletContext().getRequestDispatcher("/WEB-INF/publish.jsp").forward(request, response);	
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String publishType = request.getParameter("publishType");
		int serviceId = Integer.valueOf(request.getParameter("serviceId"));
		String contractorId = request.getParameter("contractorId");
		String announcementDate = request.getParameter("serviceTimeSlot");
		double servicePrice = Float.valueOf(request.getParameter("servicePrice"));
		String alertMessage = "";
		StorageDB mysqlServer = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
		if(publishType.equals("oneshot")) {
		try {
			Service service = mysqlServer.getService(Integer.valueOf(serviceId));
			User contractor = mysqlServer.getUserById(contractorId);
			Integer result = mysqlServer.addAnnouncement(service, announcementDate, contractor, servicePrice);
			if(result.equals(1)) {
				alertMessage = "Your timeslot has been added !";
				response.sendRedirect(request.getContextPath() + "/profile?a="+alertMessage);
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
			alertMessage = "Conflict Timeslot or 24hr time span needed !";
			request.setAttribute("alertMessage", alertMessage);
		}
		}
		doGet(request, response);
	}

}
