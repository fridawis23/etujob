package com.eservices.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.eservices.dao.Storage;
import com.eservices.dao.StorageDB;
import com.eservices.entity.Announcement;
import com.eservices.entity.Category;

@WebServlet("/home")
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public HomeServlet() {
        super();
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String alertMessage = request.getParameter("a");
		
		StorageDB mysql_server = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
		
		List<Announcement> announcement_list = null;
		List<Category> category_list=null;
		try {
			// recupere toutes les annonces de la BDD
			announcement_list = mysql_server.getAnnouncements(); 
			category_list=mysql_server.getCategory();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// Transmet une ArrayList d'annonces a la page d'accueil
		request.setAttribute("announcement_list", announcement_list);  
		request.setAttribute("category_list", category_list);
		request.setAttribute("alertMessage", alertMessage);
		this.getServletContext().getRequestDispatcher("/WEB-INF/home.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//doGet(request, response);
		
	
	}

}
