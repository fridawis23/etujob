package com.eservices.servlet;
/**
 * @startuml
 * component1 -> component2:Test
 * component2 --> component1: Test recu
 * @enduml
 * */
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.crypto.bcrypt.BCrypt;

import com.eservices.dao.StorageDB;
import com.eservices.entity.User;
import com.mysql.cj.xdevapi.Statement;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
    public LoginServlet() {
        super();
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String alertMessage = request.getParameter("a");
		request.setAttribute("alertMessage", alertMessage);
		this.getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			StorageDB mysql_server = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
			String login = request.getParameter("loginEmail");
			String password = request.getParameter("password");
			User connectedUser = mysql_server.getUserByEmail(login);
			String alertMessage = "";
			if(connectedUser.getEmail().isEmpty()) {
				String invalidData = "Login / mot de passe invalide ! ";
				request.setAttribute("invalidData", invalidData);
				doGet(request, response);
				return;
			}
			HttpSession session = request.getSession();
			
			String storedLogin = connectedUser.getEmail();
			String storedPassword = connectedUser.getPassword();
			
			
			if(BCrypt.checkpw(password,storedPassword)){
				session.setAttribute("firstname", connectedUser.getFirstname());
				session.setAttribute("lastname",connectedUser.getLastname());
				session.setAttribute("connectedId", connectedUser.getId());
				session.setAttribute("connectedEmail", connectedUser.getEmail());
				session.setAttribute("connectedStatus", connectedUser.getStatus());
				session.setAttribute("connectedCity", connectedUser.getCity());
				session.setAttribute("connectedProfile", connectedUser.getProfile());
				
				alertMessage = "Welcome!";
				/*request.setAttribute("alertMessage", alertMessage);*/
				response.sendRedirect(request.getContextPath() + "/home?a=" + alertMessage);
				
			}else {
				String invalidData = "Login / mot de passe invalide ! ";
				request.setAttribute("invalidData", invalidData);
				doGet(request, response);
			}
		
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

}
