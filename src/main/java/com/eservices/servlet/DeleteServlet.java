package com.eservices.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.eservices.dao.StorageDB;
import com.eservices.entity.Category;
import com.eservices.entity.Service;
import com.eservices.entity.User;


@WebServlet("/deleteservice")
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
    public DeleteServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		StorageDB mysql_server = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
		int service_id = Integer.parseInt(request.getParameter("id"));
		HttpSession session = request.getSession();
		int connectedStatus = (int) session.getAttribute("connectedStatus");
		
		if(connectedStatus == 1) {
			try {
				mysql_server.deleteService(service_id);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
			//request.setAttribute("serviceId", serviceId);
			response.sendRedirect(request.getContextPath() + "/manage_services");
		}
		else
			response.sendRedirect(request.getContextPath() + "/profile");
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
