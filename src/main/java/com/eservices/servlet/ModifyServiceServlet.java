package com.eservices.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.eservices.dao.StorageDB;
import com.eservices.entity.Category;
import com.eservices.entity.Service;

/**
 * Servlet implementation class ModifyServiceServlet
 */
@WebServlet("/modify_service")
public class ModifyServiceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModifyServiceServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		StorageDB mysql_server = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
		Service service = null;
		List<Category> listCategory = new ArrayList<>();
		try {
			service = mysql_server.getService(Integer.parseInt(request.getParameter("id")));
			listCategory = mysql_server.getAllCategory();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		request.setAttribute("categories", listCategory);
		request.setAttribute("service", service);
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/modify_service.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		StorageDB mysql_server = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
		Service service = new Service();
		HttpSession session = request.getSession();
		String connectedId = Integer.toString((int) session.getAttribute("connectedId"));
		
		service.setServiceId(Integer.parseInt(request.getParameter("serviceID")));
		service.setServiceTitle(request.getParameter("serviceTitle"));
		service.setServiceDescription(request.getParameter("serviceDescription"));
		service.setServiceDuration(request.getParameter("serviceDuration"));
		try {
			service.setServiceCategory(mysql_server.getCategory(Integer.parseInt(request.getParameter("serviceCategory"))));
			service.setStudent(mysql_server.getUserById(connectedId));
			mysql_server.modifyService(service);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		
		
		response.sendRedirect(request.getContextPath() + "/manage_services");
	}

}
