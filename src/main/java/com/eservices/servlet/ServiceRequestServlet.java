package com.eservices.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.eservices.dao.StorageDB;
import com.eservices.entity.Announcement;
import com.eservices.entity.User;


@WebServlet("/contrat")
public class ServiceRequestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
    public ServiceRequestServlet() {
        super();
    }
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 this.getServletContext().getRequestDispatcher("/WEB-INF/home.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String announcementId = request.getParameter("announcementId");
		String announcementTitle = request.getParameter("announcementTitle");
		String contractorId = session.getAttribute("connectedId").toString();
		String requestNote = "Bonjour, je voudrais benefier de votre service";
		String publisherId = request.getParameter("publisherId");
		String alertMessage = "";
		if(session.getAttribute("connectedStatus").toString().equals("3") || publisherId.equals(contractorId)){
			
			request.setAttribute("alertMessage","need authorized account for this option!");
			alertMessage = "You cannot contract your own service !";
			response.sendRedirect(request.getContextPath() + "/DetailAnnouncement?id="+ announcementId +"&a="+alertMessage);
			return;
		}
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
		Date currentTime = new Date(System.currentTimeMillis());
		//String validationDate = "1990-01-01 00:00:00";
		Date validationDate = new Date(System.currentTimeMillis() + TimeUnit.DAYS.toMillis(1));

		
		
		StorageDB mysqlServer = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
		try {
				mysqlServer.addServiceRequest(announcementId, contractorId, requestNote, simpleDateFormat.format(currentTime), simpleDateFormat.format(validationDate));
				Announcement announcement = mysqlServer.getAnnouncementById(Integer.valueOf(announcementId));
				//Integer receiverId = announcement.getService().getStudent().getId();
				mysqlServer.addMessage(contractorId, publisherId, requestNote, announcementTitle); 
				alertMessage = "Your request has been submitted! Use the messaging system to keep in touch";
		}catch(Exception e) {
				e.printStackTrace();
			}
		
		request.setAttribute("alertMessage", alertMessage);
		response.sendRedirect(request.getContextPath() + "/home?a="+alertMessage);
		
	}

}
