package com.eservices.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.eservices.dao.StorageDB;
import com.eservices.entity.Announcement;
import com.eservices.entity.User;
import com.eservices.helper.MailingHandler;
import com.eservices.helper.UniqueCode;

import org.apache.commons.text.RandomStringGenerator;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.scrypt.SCryptPasswordEncoder;
/**
 * Servlet implementation class PasswordForgetServlet
 */
@WebServlet("/passwordforget")
public class PasswordForgetServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PasswordForgetServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/WEB-INF/passwordforget.jsp").forward(request, response);
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String inputLoginId = request.getParameter("loginId");
		String alertMessage = "";
		StorageDB mysqlServer = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
		try {
			User recipient = mysqlServer.getUserByEmail(inputLoginId);
			if(recipient.getEmail().equals("")) {
				alertMessage = "Email doesn't exist in our database";
				request.setAttribute("alertMessage", alertMessage);
				request.setAttribute("loginId", inputLoginId);
				doGet(request, response);
				return;
			}
			
			String uniqueCode = UniqueCode.generate(6);
			String uniqueCodegeneratedHash = BCrypt.hashpw(uniqueCode, BCrypt.gensalt(12));
			uniqueCodegeneratedHash += "/";
			StorageDB mysql_server = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
				Date deadline = new Date(System.currentTimeMillis() + TimeUnit.DAYS.toMillis(1));
				mysql_server.addPasswordResetRequest(inputLoginId, uniqueCodegeneratedHash, sdf.format(deadline)); 
				
				
			} catch (Exception e) {
				e.printStackTrace();
				
			}
			
			StringBuilder msgContent = new StringBuilder();
			msgContent.append("Dear " + recipient.getFirstname()+ " " + recipient.getLastname() + ", "
			+ "\n we have received a password reset for your account.\n" 
			+ "If that was your request, please use the link below within the next 24 hours, "
			+ "otherwise please ignore this message.\n");
			msgContent.append("http://localhost:8080/EtuJob/passwordreset?id=" + recipient.getId() 
			+ "&c=" + uniqueCodegeneratedHash);
			
			MailingHandler.sendEmail(recipient.getEmail(), " Etu'Job | Password Reset", msgContent.toString());
			
			alertMessage = "Reset link has been sent to you via email, please check your mailbox!";
			//request.setAttribute("alertMessage", alertMessage);
			request.setAttribute("loginId", inputLoginId);
			response.sendRedirect(request.getContextPath() + "/login?a="+ alertMessage);
			 return;
		} catch (Exception e) {
			e.printStackTrace();
			// alertMessage = "An error Occured";
			request.setAttribute("alertMessage", e.toString());
			doGet(request, response);
		}
		alertMessage = "An error Occured";
		request.setAttribute("alertMessage", alertMessage);
		doGet(request, response);
	}
}
