package com.eservices.entity;

public class Announcement {
	
	private int announcementId;
	private Service service;
	private User contractor;
	private String announcementDate;
	private String publishedDate;
	private double servicePrice;
	
	public String getPublished_date() {
		return publishedDate;
	}
	public void setPublished_date(String published_date) {
		this.publishedDate = published_date;
	}
	public int getAnnouncementId() {
		return announcementId;
	}
	public void setAnnouncementId(int announcementId) {
		this.announcementId = announcementId;
	}
	public Service getService() {
		return service;
	}
	public void setService(Service service) {
		this.service = service;
	}
	public User getContractor() {
		return contractor;
	}
	public void setContractor(User contractor) {
		this.contractor = contractor;
	}
	public String getAnnouncement_date() {
		return announcementDate;
	}
	public void setAnnouncement_date(String announcement_date) {
		this.announcementDate = announcement_date;
	}
	public double getService_price() {
		return servicePrice;
	}
	public void setService_price(double service_price) {
		this.servicePrice = service_price;
	}
	
}
