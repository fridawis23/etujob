package com.eservices.entity;

public class Service {
	
	private int serviceId;
	private User student;
	private String serviceTitle;
	private String serviceDescription;
	private String serviceDuration;
	private Category serviceCategory;
	
	public int getServiceId() {
		return serviceId;
	}
	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}
	public User getStudent() {
		return student;
	}
	public void setStudent(User student) {
		this.student = student;
	}
	public String getServiceTitle() {
		return serviceTitle;
	}
	public void setServiceTitle(String service_title) {
		this.serviceTitle = service_title;
	}
	public String getServiceDescription() {
		return serviceDescription;
	}
	public void setServiceDescription(String service_description) {
		this.serviceDescription = service_description;
	}
	public String getServiceDuration() {
		return serviceDuration;
	}
	public void setServiceDuration(String service_duration) {
		this.serviceDuration = service_duration;
	}
	public Category getServiceCategory() {
		return serviceCategory;
	}
	public void setServiceCategory(Category service_category) {
		this.serviceCategory = service_category;
	}
	
	
}

