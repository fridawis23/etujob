package com.eservices.entity;

public class Comment {
	private int commentId;
	private Service service;
	private User student;
	private String comment_date_time;
	private String comment_content;
	
	
	public int getCommentId() {
		return commentId;
	}


	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}

	public Service getService() {
		return service;
	}


	public void setService(Service service) {
		this.service = service;
	}


	public User getStudent() {
		return student;
	}


	public void setStudent(User student) {
		this.student = student;
	}


	public String getComment_date_time() {
		return comment_date_time;
	}


	public void setComment_date_time(String comment_date_time) {
		this.comment_date_time = comment_date_time;
	}


	public String getComment_content() {
		return comment_content;
	}


	public void setComment_content(String comment_content) {
		this.comment_content = comment_content;
	}


	
	
	

}
