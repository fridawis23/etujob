package com.eservices.entity;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Request {
	private Announcement announcement;
	private User contractor;
	private String requestNote;
	private Date requestDate;
	private Date validationDate;
	public Announcement getAnnouncement() {
		return announcement;
	}
	public void setAnnouncement(Announcement announcement) {
		this.announcement = announcement;
	}
	public User getContractor() {
		return contractor;
	}
	public void setContractor(User contractor) {
		this.contractor = contractor;
	}
	public String getRequestNote() {
		return requestNote;
	}
	public void setRequestNote(String requestNote) {
		this.requestNote = requestNote;
	}
	public Date getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}
	public Date getValidationDate() {
		return validationDate;
	}
	public void setValidationDate(Date validationDate) {
		this.validationDate = validationDate;
	}
	
	
}
