package com.eservices.account;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.crypto.bcrypt.BCrypt;

import com.eservices.dao.StorageDB;
import com.eservices.entity.User;
import com.eservices.helper.MailingHandler;
import com.eservices.helper.UniqueCode;


@WebServlet("/register")
public class RegisterServlet extends HttpServlet {
	
	private static final int REGISTER_SUCCESSFUL = 1;
	private static final int REGISTER_FAILED = 0;
	private static final long serialVersionUID = 1L;
	
	private String lastname;
	private String firstname;
	private String email;
	private String password1;
	private String password2;
	private int status;
	private String profile;
	private String phone;
	private String photo;
	private String city;
   
    public RegisterServlet() {
        super();
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/WEB-INF/register.jsp").forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// doGet(request, response);
		this.lastname = request.getParameter("lastname");
		this.firstname = request.getParameter("firstname");
		this.email = request.getParameter("email");
		this.password1 = request.getParameter("password1");
		this.password2 = request.getParameter("password2");
		this.status = Integer.valueOf(request.getParameter("status"));
		this.profile = request.getParameter("profile");
		this.phone = request.getParameter("phone");
		this.photo = request.getParameter("photo");
		this.city = request.getParameter("city");
		
		StorageDB mysql_server = new StorageDB("jdbc:mysql://localhost:3306/etujob_data", "philbay", "Crimson2022$");
		User recipient = new User();
		String alertMessage = "";
		int result = 0;
		try {
			
			if(!password1.equals(password2)) {
				showMessage(request,response,"Passwords mismatched !");
				doGet(request, response);
				return;
			}
			
			recipient = mysql_server.getUserByEmail(email);
			if(!recipient.getEmail().isEmpty()) {
				alertMessage = "Email already taken! ";
				showMessage(request, response, alertMessage);
				doGet(request, response);
				return;
			}
			
			
			User user = new User();
			user.setLastname(lastname);
			user.setFirstname(firstname);
			user.setEmail(email);
			String passwordToStore = BCrypt.hashpw(password1, BCrypt.gensalt());
			user.setPassword(passwordToStore);
			user.setStatus(status);
			user.setProfile(profile);
			user.setPhone(phone);
			user.setPhoto(photo);
			user.setCity(city);
			
			result = mysql_server.addUser(user);
		
			request.setAttribute("result", result);
			
			if(result == REGISTER_SUCCESSFUL) {
				
				// generate token for email validation
				String uniqueCode = UniqueCode.generate(6);
				String uniqueCodegeneratedHash = BCrypt.hashpw(uniqueCode, BCrypt.gensalt(12));
				uniqueCodegeneratedHash += "/";
				
				
					recipient = mysql_server.getUserByEmail(email);
					// storing the token in the database
					mysql_server.addEmailValidationRequest(recipient.getId(), uniqueCodegeneratedHash, "1900-01-01 00:00:00", "2"); 
				
				
				// build email
				StringBuilder msgContent = new StringBuilder();
				msgContent.append("Dear " + firstname + " " + lastname + ", "
				+ "\n Your account has been created successfully.\n" 
				+ "Please use the link below to activate your profile and fully enjoy Etu'Job.\n");
				msgContent.append("http://172.23.194.82:8080/EtuJob/accountvalidation?id=" + recipient.getId() 
				+ "&c=" + uniqueCodegeneratedHash);
				msgContent.append("\nWith regards\n");
				// send email
				String message = msgContent.toString();
				String sender = recipient.getEmail();
				MailingHandler.sendEmail(sender, " Etu'Job | Activation link", message);
				
				// sending notification to login
				alertMessage = "Email sent to your mailbox for validation";
				request.setAttribute("alertMessage", alertMessage);
				response.sendRedirect(request.getContextPath() + "/login?a="+alertMessage);
				return;
			}else {
					/*try {
						showMessage(request,response," Email already taken !");
						doGet(request, response);
					} catch (Exception e) {
						e.printStackTrace();
					}*/
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		
		
	}

	private void showMessage(HttpServletRequest request, HttpServletResponse response, String alertMessage) throws Exception {
		request.setAttribute("lastname", lastname);
		request.setAttribute("firstname", firstname);
		request.setAttribute("email", email);
		request.setAttribute("city", city);
		request.setAttribute("password1", password1);
		request.setAttribute("password2", password2);
		request.setAttribute("profile", profile);
		request.setAttribute("phone", phone);
		request.setAttribute("photo", photo);
		request.setAttribute("alertMessage", alertMessage);
	}

}
